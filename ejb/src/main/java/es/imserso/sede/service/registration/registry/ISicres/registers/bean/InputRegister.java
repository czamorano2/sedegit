package es.imserso.sede.service.registration.registry.ISicres.registers.bean;

import javax.xml.datatype.XMLGregorianCalendar;

import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSInputRegister;
import es.imserso.sede.service.registration.registry.bean.InputRegisterI;

/**
 * Bean que contiene la información devuelta por el registro al obtener una
 * entrada.
 * 
 * @author 11825775
 *
 */
public class InputRegister extends OutputRegister implements InputRegisterI {

	protected String originalNumber;
	protected XMLGregorianCalendar originalDate;
	protected Integer originalType;
	protected String originalEntity;
	protected String originalEntityName;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterI#
	 * getOriginalNumber()
	 */
	@Override
	public String getOriginalNumber() {
		return originalNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterI#
	 * setOriginalNumber(java.lang.String)
	 */
	@Override
	public void setOriginalNumber(String originalNumber) {
		this.originalNumber = originalNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterI#
	 * getOriginalDate()
	 */
	@Override
	public XMLGregorianCalendar getOriginalDate() {
		return originalDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterI#
	 * setOriginalDate(javax.xml.datatype.XMLGregorianCalendar)
	 */
	@Override
	public void setOriginalDate(XMLGregorianCalendar originalDate) {
		this.originalDate = originalDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterI#
	 * getOriginalType()
	 */
	@Override
	public Integer getOriginalType() {
		return originalType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterI#
	 * setOriginalType(java.lang.Integer)
	 */
	@Override
	public void setOriginalType(Integer originalType) {
		this.originalType = originalType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterI#
	 * getOriginalEntity()
	 */
	@Override
	public String getOriginalEntity() {
		return originalEntity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterI#
	 * setOriginalEntity(java.lang.String)
	 */
	@Override
	public void setOriginalEntity(String originalEntity) {
		this.originalEntity = originalEntity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterI#
	 * getOriginalEntityName()
	 */
	@Override
	public String getOriginalEntityName() {
		return originalEntityName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterI#
	 * setOriginalEntityName(java.lang.String)
	 */
	@Override
	public void setOriginalEntityName(String originalEntityName) {
		this.originalEntityName = originalEntityName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.bean.InputRegisterI#
	 * toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("");
		sb.append("original number: " + this.originalNumber + ";original type: " + this.originalType
				+ ", original entity: " + this.originalEntity + ", original entity name: " + this.originalEntityName
				+ ", original date: " + this.originalDate);
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.ISicres.registers.bean.Register#
	 * importData(java.lang.Object)
	 */
	@Override
	public void importData(Object o) {
		if (o instanceof WSInputRegister) {
			WSInputRegister ws = (WSInputRegister) o;
			super.importData(o);

			setOriginalNumber(ws.getOriginalNumber());
			setOriginalDate(ws.getOriginalDate());
			setOriginalType(ws.getOriginalType());
			setOriginalEntity(ws.getOriginalEntity());
			setOriginalEntityName(ws.getOriginalEntityName());
		}
	}

}

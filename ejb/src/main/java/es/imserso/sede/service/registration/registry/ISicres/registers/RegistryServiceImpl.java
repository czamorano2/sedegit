package es.imserso.sede.service.registration.registry.ISicres.registers;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.inject.Inject;
import javax.xml.namespace.QName;

import org.jboss.logging.Logger;

import es.imserso.hermes.session.webservice.dto.DocumentoRegistradoI;
import es.imserso.hermes.session.webservice.dto.TipoDocumentoRegistrado;
import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.service.registration.RegistrationException;
import es.imserso.sede.service.registration.registry.RegistryUtils;
import es.imserso.sede.service.registration.registry.ISicres.registers.bean.InputRegister;
import es.imserso.sede.service.registration.registry.ISicres.registers.bean.InputRegisterResponse;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.ArrayOfWSParamDocument;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.ISWebServiceRegistersSoap;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.ISWebServiceRegistersSoapImplService;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.RegistersObjectFactory;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.Security;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.UsernameTokenClass;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSAttachPageEx;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSGetPage;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSGetPageResponse;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSLoadInputRegisterFromId;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSLoadInputRegisterFromIdResponse;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSNewInputRegister;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSNewInputRegisterResponse;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSParamDocument;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSParamInputRegisterEx;
import es.imserso.sede.service.registration.registry.bean.InputRegisterI;
import es.imserso.sede.service.registration.registry.bean.InputRegisterResponseI;
import es.imserso.sede.util.Utils;

/**
 * Implementación para InveSicres.
 * 
 * @author 11825775
 *
 */
public class RegistryServiceImpl implements RegistryServiceI {

	private QName ISWEBSERVICEREGISTERSSOAPIMPLSERVICE_QNAME;
	private URL ISWEBSERVICEREGISTERSSOAPIMPLSERVICE_WSDL_LOCATION;

	@Inject
	private Logger log;

	@Inject
	private RegistersObjectFactory objectFactory;

	@Inject
	private PropertyComponent propertyComponent;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.RegistryServiceI#
	 * registerNewSolicitud(java.util.List, byte[])
	 */
	@Override
	public InputRegisterResponseI registerNewSolicitud(int bookIdentification, List<DocumentoRegistradoI> adjuntos,
			byte[] solicitudPdf, long solicitudId, String codigoSIA) throws RegistrationException {
		log.info("se va a registrar una nueva solicitud ...");

		InputRegisterResponseI newRegisterResponse = null;

		ISWebServiceRegistersSoap pService = getRegistersService();

		ArrayOfWSParamDocument arrayOfWSParamDocument = objectFactory.createArrayOfWSParamDocument();

		String nombrePdfSolicitud = RegistryUtils.getNombrePdfSolicitud(solicitudId, codigoSIA);

		// añadimos el pdf con los datos de la solicitud
		WSParamDocument wsParamDocument = new WSParamDocument();
		wsParamDocument.setDocumentName(TipoDocumentoRegistrado.SOLICITUD.getNombreDocumentoPorDefecto());
		wsParamDocument.setFileName(nombrePdfSolicitud);
		wsParamDocument.setDocumentContent(solicitudPdf);
		arrayOfWSParamDocument.getWSParamDocument().add(wsParamDocument);

		// Añadimos los documentos adjuntos
		for (DocumentoRegistradoI attachedFile : adjuntos) {
			wsParamDocument = new WSParamDocument();
			wsParamDocument.setDocumentName(attachedFile.getNombreDocumento());
			wsParamDocument.setFileName(attachedFile.getNombreFichero());
			wsParamDocument.setDocumentContent(attachedFile.getFichero());
			arrayOfWSParamDocument.getWSParamDocument().add(wsParamDocument);
		}

		UsernameTokenClass usernameToken = new UsernameTokenClass();
		usernameToken.setUsername(propertyComponent.getiSicresUsername());
		usernameToken.setPassword(propertyComponent.getiSicresPassword());
		Security security = new Security();
		security.setUsernameToken(usernameToken);

		WSParamInputRegisterEx wsParamInputRegisterEx = objectFactory.createWSParamInputRegisterEx();

		WSNewInputRegister wsNewInputRegister = new WSNewInputRegister();

		WSNewInputRegisterResponse wsNewInputRegisterResponse = null;

		try {

			wsNewInputRegister.setBookIdentification(bookIdentification);
			wsParamInputRegisterEx.setSender(propertyComponent.getiSicresSender());
			wsParamInputRegisterEx.setDestination(propertyComponent.getiSicresDestination());
			wsParamInputRegisterEx.setTransportType(propertyComponent.getiSicresTransportType());
			wsParamInputRegisterEx.setTransportNumber(propertyComponent.getiSicresTransportNumber());
			wsParamInputRegisterEx.setMatterType(propertyComponent.getiSicresMatterType());
			wsParamInputRegisterEx.setDocuments(arrayOfWSParamDocument);

			wsNewInputRegister.setDatas(wsParamInputRegisterEx);

			wsNewInputRegisterResponse = pService.wsNewInputRegister(wsNewInputRegister, security);

			log.info("se ha registrado la solicitud con número de registro ");

			newRegisterResponse = new InputRegisterResponse();
			newRegisterResponse.setNumber(wsNewInputRegisterResponse.getWSNewInputRegisterResult().getNumber());
			newRegisterResponse.setFolderId(wsNewInputRegisterResponse.getWSNewInputRegisterResult().getFolderId());
			newRegisterResponse.setBookId(wsNewInputRegisterResponse.getWSNewInputRegisterResult().getBookId());
			newRegisterResponse.setDate(wsNewInputRegisterResponse.getWSNewInputRegisterResult().getDate());
			newRegisterResponse.setOffice(wsNewInputRegisterResponse.getWSNewInputRegisterResult().getOffice());
			newRegisterResponse.setOfficeName(wsNewInputRegisterResponse.getWSNewInputRegisterResult().getOfficeName());
			newRegisterResponse.setUserName(wsNewInputRegisterResponse.getWSNewInputRegisterResult().getUserName());

		} catch (Exception e) {
			String errmsg = "error al guardar la solicitud con id=" + solicitudId + " y SIA=" + codigoSIA + ": "
					+ Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new RegistrationException(errmsg);
		}
		return newRegisterResponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.RegistryServiceI#
	 * addDocumentToSolicitud(java.lang.String, byte[])
	 */
	@Override
	public void addDocumentToSolicitud(int bookIdentification, String documentName, String fileName,
			int registrationNumber, byte[] document) throws RegistrationException {

		log.debug("se va a adjuntar el documento a la solicitud con el número de registro: " + registrationNumber);

		ISWebServiceRegistersSoap pService = getRegistersService();

		ArrayOfWSParamDocument arrayOfWSParamDocument = objectFactory.createArrayOfWSParamDocument();

		WSParamDocument wsParamDocument = new WSParamDocument();
		wsParamDocument.setDocumentName(documentName);
		wsParamDocument.setFileName(fileName);
		wsParamDocument.setDocumentContent(document);
		arrayOfWSParamDocument.getWSParamDocument().add(wsParamDocument);

		WSAttachPageEx wsAttachPage = new WSAttachPageEx();
		wsAttachPage.setBookIdentification(bookIdentification);
		wsAttachPage.setRegisterIdentification(registrationNumber);
		wsAttachPage.setDocuments(arrayOfWSParamDocument);

		UsernameTokenClass usernameToken = new UsernameTokenClass();
		usernameToken.setUsername(propertyComponent.getiSicresUsername());
		usernameToken.setPassword(propertyComponent.getiSicresPassword());
		Security security = new Security();
		security.setUsernameToken(usernameToken);

		try {
			pService.wsAttachPageEx(wsAttachPage, security);
			log.info("se ha adjuntado el documento a la solicitud con el número de registro: " + registrationNumber);

		} catch (Exception e) {
			String errmsg = "error al adjuntar el documento a la solicitud con el número de registro "
					+ registrationNumber + ": " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new RegistrationException(errmsg);

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.RegistryServiceI#
	 * getRegistry(int)
	 */
	@Override
	public InputRegisterI getInputRegister(int bookIdentification, int registrationNumber)
			throws RegistrationException {
		InputRegisterI inputRegister = null;

		log.info("se va a obtener la información del número de registro: " + registrationNumber);

		ISWebServiceRegistersSoap pService = getRegistersService();

		WSLoadInputRegisterFromId wSLoadInputRegisterFromId = new WSLoadInputRegisterFromId();
		wSLoadInputRegisterFromId.setBookIdentification(bookIdentification);
		wSLoadInputRegisterFromId.setRegisterIdentification(registrationNumber);

		UsernameTokenClass usernameToken = new UsernameTokenClass();
		usernameToken.setUsername(propertyComponent.getiSicresUsername());
		usernameToken.setPassword(propertyComponent.getiSicresPassword());
		Security security = new Security();
		security.setUsernameToken(usernameToken);

		WSLoadInputRegisterFromIdResponse wsLoadInputRegisterFromIdResponse = null;
		try {
			wsLoadInputRegisterFromIdResponse = pService.wsLoadInputRegisterFromId(wSLoadInputRegisterFromId, security);
			log.info("se ha obtenido la información del número de registro " + registrationNumber);

			inputRegister = new InputRegister();
			inputRegister.setOriginalNumber(
					wsLoadInputRegisterFromIdResponse.getWSLoadInputRegisterFromIdResult().getOriginalNumber());
			inputRegister.setOriginalType(
					wsLoadInputRegisterFromIdResponse.getWSLoadInputRegisterFromIdResult().getOriginalType());
			inputRegister.setOriginalDate(
					wsLoadInputRegisterFromIdResponse.getWSLoadInputRegisterFromIdResult().getOriginalDate());
			inputRegister.setOriginalEntity(
					wsLoadInputRegisterFromIdResponse.getWSLoadInputRegisterFromIdResult().getOriginalEntity());
			inputRegister.setOriginalEntityName(
					wsLoadInputRegisterFromIdResponse.getWSLoadInputRegisterFromIdResult().getOriginalEntityName());
			inputRegister.setDocuments(
					wsLoadInputRegisterFromIdResponse.getWSLoadInputRegisterFromIdResult().getDocuments());

		} catch (Exception e) {
			String errmsg = "error al obtener el registro de entrada (id=" + registrationNumber + "): "
					+ Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new RegistrationException(errmsg);

		}

		return inputRegister;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.RegistryServiceI#
	 * getSolicitud(int)
	 */
	@Override
	public byte[] getAttachedDocument(int bookIdentification, int registrationNumber, int documentIndex, int pageIndex)
			throws RegistrationException {

		log.info("se va a obtener el documento adjunto con número de registro: " + registrationNumber
				+ ", índice de documento: " + documentIndex + ", índice de página:" + pageIndex);

		ISWebServiceRegistersSoap pService = getRegistersService();

		WSGetPage wsGetPage = new WSGetPage();
		wsGetPage.setBookIdentification(bookIdentification);
		wsGetPage.setRegisterIdentification(registrationNumber);
		wsGetPage.setDocumentIndex(documentIndex);
		wsGetPage.setPageIndex(pageIndex);

		UsernameTokenClass usernameToken = new UsernameTokenClass();
		usernameToken.setUsername(propertyComponent.getiSicresUsername());
		usernameToken.setPassword(propertyComponent.getiSicresPassword());
		Security security = new Security();
		security.setUsernameToken(usernameToken);

		WSGetPageResponse wsGetPageResponse = null;
		try {
			wsGetPageResponse = pService.wsGetPage(wsGetPage, security);
			log.info("se ha obtenido el documento adjunto!");

		} catch (Exception e) {
			String errmsg = "error al obtener el documento adjunto: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new RegistrationException(errmsg);

		}
		return wsGetPageResponse.getWSGetPageResult();

	}

	private ISWebServiceRegistersSoap getRegistersService() throws RegistrationException {

		ISWebServiceRegistersSoapImplService is = new ISWebServiceRegistersSoapImplService(
				getISWebServiceRegistersSoapWsdlLocation(), getISWebServiceRegistersSoapQName());
		return is.getISWebServiceRegistersSoapImplPort();
	}

	private QName getISWebServiceRegistersSoapQName() {
		if (ISWEBSERVICEREGISTERSSOAPIMPLSERVICE_QNAME == null) {
			ISWEBSERVICEREGISTERSSOAPIMPLSERVICE_QNAME = new QName(propertyComponent.getiSicresRegistersNamespaceURI(),
					propertyComponent.getiSicresRegistersQNameLocalPart());
		}
		return ISWEBSERVICEREGISTERSSOAPIMPLSERVICE_QNAME;
	}

	private URL getISWebServiceRegistersSoapWsdlLocation() throws RegistrationException {
		if (ISWEBSERVICEREGISTERSSOAPIMPLSERVICE_WSDL_LOCATION == null) {
			try {
				ISWEBSERVICEREGISTERSSOAPIMPLSERVICE_WSDL_LOCATION = new URL(
						propertyComponent.getiSicresRegistersWsdlUrl());
			} catch (MalformedURLException ex) {
				throw new RegistrationException("No se pudo obtener el fichero WSDL:" + Utils.getExceptionMessage(ex));
			}
		}
		return ISWEBSERVICEREGISTERSSOAPIMPLSERVICE_WSDL_LOCATION;
	}

}

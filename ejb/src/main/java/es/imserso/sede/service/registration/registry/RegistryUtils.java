package es.imserso.sede.service.registration.registry;

import es.imserso.sede.util.exception.SedeRuntimeException;

public class RegistryUtils {

	private static final String REGISTRY_BOOK_PREFIX = "sede";

	/**
	 * solicitudSIA_numeroRegistro_id.pdf (id es el atributo id de la entidad
	 * Solicitud de la base de datos de la Sede)
	 */
	private static final String PATRON_NOMBRE_FICHERO_PDF_SOLICITUD = "solicitudSIA_%s_%s.pdf";

	/**
	 * justificante_numeroRegistro_id.pdf (id es el atributo id de la entidad
	 * Solicitud de la base de datos de la Sede)
	 */
	private static final String PATRON_NOMBRE_FICHERO_PDF_JUSTIFICANTE = "justificante_%s_%s.pdf";

	/**
	 * @param year
	 *            año del libro de entrada
	 * @return nombre del libro en el registro de ISicres
	 */
	public static String getBookName(int year) {
		String y = String.valueOf(year);
		if (y.length() != 4) {
			throw new SedeRuntimeException("El año debe tener 4 dígitos");
		}

		// FIXME mockeamos el libro de 2016 porque no tiene el prefijo 'sede'
		if (year == 2016) {
			return "PRUEBA ENTRADA 2016";
		}
		return REGISTRY_BOOK_PREFIX.concat(y);
	}

	/**
	 * @param numeroRegistro
	 *            número de registro completo de la solicitud (ej:
	 *            201620000000179)
	 * @return año del libro de entrada de la solicitud
	 */
	public static int getBookYearFromNumeroRegistro(String numeroRegistro) {
		return Integer.parseInt(numeroRegistro.substring(0, 4));
	}

	/**
	 * @param solicitudId
	 *            es el atributo id de la entidad Solicitud de la base de datos
	 *            de la Sede
	 * @param numeroRegistro
	 *            número de registro completo de la solicitud (ej:
	 *            201620000000179)
	 * @return nombre del fichero pdf que contiene la solicitud
	 */
	public static String getNombrePdfSolicitud(Long solicitudId, String numeroRegistro) {
		return String.format(PATRON_NOMBRE_FICHERO_PDF_SOLICITUD, solicitudId.toString(), numeroRegistro);
	}

	/**
	 * @param solicitudId
	 *            es el atributo id de la entidad Solicitud de la base de datos
	 *            de la Sede
	 * @param numeroRegistro
	 *            número de registro completo de la solicitud (ej:
	 *            201620000000179)
	 * @return nombre del fichero pdf que contiene la solicitud
	 */
	public static String getNombreJustificante(Long solicitudId, String numeroRegistro) {
		return String.format(PATRON_NOMBRE_FICHERO_PDF_JUSTIFICANTE, solicitudId.toString(), numeroRegistro);
	}

}

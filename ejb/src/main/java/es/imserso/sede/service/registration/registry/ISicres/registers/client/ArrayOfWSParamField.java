
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfWSParamField complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWSParamField">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WSParamField" type="{http://www.invesicres.org}WSParamField" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWSParamField", propOrder = {
    "wsParamField"
})
public class ArrayOfWSParamField {

    @XmlElement(name = "WSParamField", nillable = true)
    protected List<WSParamField> wsParamField;

    /**
     * Gets the value of the wsParamField property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsParamField property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWSParamField().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WSParamField }
     * 
     * 
     */
    public List<WSParamField> getWSParamField() {
        if (wsParamField == null) {
            wsParamField = new ArrayList<WSParamField>();
        }
        return this.wsParamField;
    }

}

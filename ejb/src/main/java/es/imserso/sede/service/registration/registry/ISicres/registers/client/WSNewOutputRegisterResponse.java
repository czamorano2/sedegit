
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WSNewOutputRegisterResult" type="{http://www.invesicres.org}WSRegister" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsNewOutputRegisterResult"
})
@XmlRootElement(name = "WSNewOutputRegisterResponse")
public class WSNewOutputRegisterResponse {

    @XmlElement(name = "WSNewOutputRegisterResult")
    protected WSRegister wsNewOutputRegisterResult;

    /**
     * Obtiene el valor de la propiedad wsNewOutputRegisterResult.
     * 
     * @return
     *     possible object is
     *     {@link WSRegister }
     *     
     */
    public WSRegister getWSNewOutputRegisterResult() {
        return wsNewOutputRegisterResult;
    }

    /**
     * Define el valor de la propiedad wsNewOutputRegisterResult.
     * 
     * @param value
     *     allowed object is
     *     {@link WSRegister }
     *     
     */
    public void setWSNewOutputRegisterResult(WSRegister value) {
        this.wsNewOutputRegisterResult = value;
    }

}

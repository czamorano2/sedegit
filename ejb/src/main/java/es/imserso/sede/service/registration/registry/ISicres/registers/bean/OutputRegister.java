package es.imserso.sede.service.registration.registry.ISicres.registers.bean;

import es.imserso.sede.service.registration.registry.ISicres.registers.client.ArrayOfWSAddField;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.ArrayOfWSDocument;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.ArrayOfWSPerson;
import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSOutputRegister;
import es.imserso.sede.service.registration.registry.bean.OutputRegisterI;

public class OutputRegister extends Register implements OutputRegisterI {

	protected String sender;
	protected String senderName;
	protected String destination;
	protected String destinationName;
	protected String transportType;
	protected String transportNumber;
	protected String matterType;
	protected String matterTypeName;
	protected String matter;
	protected ArrayOfWSPerson persons;
	protected ArrayOfWSDocument documents;
	protected ArrayOfWSAddField addFields;

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#getSender()
	 */
	@Override
	public String getSender() {
		return sender;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#setSender(java.lang.String)
	 */
	@Override
	public void setSender(String sender) {
		this.sender = sender;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#getSenderName()
	 */
	@Override
	public String getSenderName() {
		return senderName;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#setSenderName(java.lang.String)
	 */
	@Override
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#getDestination()
	 */
	@Override
	public String getDestination() {
		return destination;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#setDestination(java.lang.String)
	 */
	@Override
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#getDestinationName()
	 */
	@Override
	public String getDestinationName() {
		return destinationName;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#setDestinationName(java.lang.String)
	 */
	@Override
	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#getTransportType()
	 */
	@Override
	public String getTransportType() {
		return transportType;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#setTransportType(java.lang.String)
	 */
	@Override
	public void setTransportType(String transportType) {
		this.transportType = transportType;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#getTransportNumber()
	 */
	@Override
	public String getTransportNumber() {
		return transportNumber;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#setTransportNumber(java.lang.String)
	 */
	@Override
	public void setTransportNumber(String transportNumber) {
		this.transportNumber = transportNumber;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#getMatterType()
	 */
	@Override
	public String getMatterType() {
		return matterType;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#setMatterType(java.lang.String)
	 */
	@Override
	public void setMatterType(String matterType) {
		this.matterType = matterType;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#getMatterTypeName()
	 */
	@Override
	public String getMatterTypeName() {
		return matterTypeName;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#setMatterTypeName(java.lang.String)
	 */
	@Override
	public void setMatterTypeName(String matterTypeName) {
		this.matterTypeName = matterTypeName;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#getMatter()
	 */
	@Override
	public String getMatter() {
		return matter;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#setMatter(java.lang.String)
	 */
	@Override
	public void setMatter(String matter) {
		this.matter = matter;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#getPersons()
	 */
	@Override
	public ArrayOfWSPerson getPersons() {
		return persons;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#setPersons(es.imserso.sede.service.registration.registry.ISicres.client.ArrayOfWSPerson)
	 */
	@Override
	public void setPersons(ArrayOfWSPerson persons) {
		this.persons = persons;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#getDocuments()
	 */
	@Override
	public ArrayOfWSDocument getDocuments() {
		return documents;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#setDocuments(es.imserso.sede.service.registration.registry.ISicres.client.ArrayOfWSDocument)
	 */
	@Override
	public void setDocuments(ArrayOfWSDocument documents) {
		this.documents = documents;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#getAddFields()
	 */
	@Override
	public ArrayOfWSAddField getAddFields() {
		return addFields;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.bean.OutputRegisterI#setAddFields(es.imserso.sede.service.registration.registry.ISicres.client.ArrayOfWSAddField)
	 */
	@Override
	public void setAddFields(ArrayOfWSAddField addFields) {
		this.addFields = addFields;
	}
	
	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.registry.ISicres.registers.bean.Register#importData(java.lang.Object)
	 */
	@Override
	public void importData(Object o) {
		if (o instanceof WSOutputRegister) {
			WSOutputRegister ws = (WSOutputRegister) o;
			super.importData(o);
			
			setSender(ws.getSender());
			setSenderName(ws.getSenderName());
			setDestination(ws.getDestination());
			setDestinationName(ws.getDestinationName());
			setTransportType(ws.getTransportType());
			setTransportNumber(ws.getTransportNumber());
			setMatter(ws.getMatter());
			setMatterType(ws.getMatterType());
			setMatterTypeName(ws.getMatterTypeName());
			setPersons(ws.getPersons());
			setDocuments(ws.getDocuments());
			setAddFields(ws.getAddFields());
			setFolderId(ws.getFolderId());
			setState(ws.getState());
		}

	}

}

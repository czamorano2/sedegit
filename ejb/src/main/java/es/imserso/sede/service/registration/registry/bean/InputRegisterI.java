package es.imserso.sede.service.registration.registry.bean;

import javax.xml.datatype.XMLGregorianCalendar;

public interface InputRegisterI extends OutputRegisterI {

	String getOriginalNumber();

	void setOriginalNumber(String originalNumber);

	XMLGregorianCalendar getOriginalDate();

	void setOriginalDate(XMLGregorianCalendar originalDate);

	Integer getOriginalType();

	void setOriginalType(Integer originalType);

	String getOriginalEntity();

	void setOriginalEntity(String originalEntity);

	String getOriginalEntityName();

	void setOriginalEntityName(String originalEntityName);

}
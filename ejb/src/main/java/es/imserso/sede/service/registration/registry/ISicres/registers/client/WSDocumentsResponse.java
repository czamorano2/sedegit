
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para WSDocumentsResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="WSDocumentsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="List" type="{http://www.invesicres.org}ArrayOfWSDocument" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Total" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSDocumentsResponse", propOrder = {
    "list"
})
public class WSDocumentsResponse {

    @XmlElement(name = "List")
    protected ArrayOfWSDocument list;
    @XmlAttribute(name = "Total", required = true)
    protected long total;

    /**
     * Obtiene el valor de la propiedad list.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSDocument }
     *     
     */
    public ArrayOfWSDocument getList() {
        return list;
    }

    /**
     * Define el valor de la propiedad list.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSDocument }
     *     
     */
    public void setList(ArrayOfWSDocument value) {
        this.list = value;
    }

    /**
     * Obtiene el valor de la propiedad total.
     * 
     */
    public long getTotal() {
        return total;
    }

    /**
     * Define el valor de la propiedad total.
     * 
     */
    public void setTotal(long value) {
        this.total = value;
    }

}

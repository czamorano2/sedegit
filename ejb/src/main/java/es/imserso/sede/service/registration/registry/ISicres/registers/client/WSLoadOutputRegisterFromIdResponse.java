
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WSLoadOutputRegisterFromIdResult" type="{http://www.invesicres.org}WSOutputRegister" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsLoadOutputRegisterFromIdResult"
})
@XmlRootElement(name = "WSLoadOutputRegisterFromIdResponse")
public class WSLoadOutputRegisterFromIdResponse {

    @XmlElement(name = "WSLoadOutputRegisterFromIdResult")
    protected WSOutputRegister wsLoadOutputRegisterFromIdResult;

    /**
     * Obtiene el valor de la propiedad wsLoadOutputRegisterFromIdResult.
     * 
     * @return
     *     possible object is
     *     {@link WSOutputRegister }
     *     
     */
    public WSOutputRegister getWSLoadOutputRegisterFromIdResult() {
        return wsLoadOutputRegisterFromIdResult;
    }

    /**
     * Define el valor de la propiedad wsLoadOutputRegisterFromIdResult.
     * 
     * @param value
     *     allowed object is
     *     {@link WSOutputRegister }
     *     
     */
    public void setWSLoadOutputRegisterFromIdResult(WSOutputRegister value) {
        this.wsLoadOutputRegisterFromIdResult = value;
    }

}

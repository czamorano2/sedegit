package es.imserso.sede.service.registration.registry.ISicres.registers.bean;

import javax.xml.datatype.XMLGregorianCalendar;

import es.imserso.sede.service.registration.registry.ISicres.registers.client.WSRegister;
import es.imserso.sede.service.registration.registry.bean.RegisterI;

public class Register implements RegisterI {

	protected String number;
	protected XMLGregorianCalendar date;
	protected String userName;
	protected XMLGregorianCalendar systemDate;
	protected String office;
	protected String officeName;
	protected int bookId;
	protected int folderId;
	protected int state;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.bean.RegisterI#getNumber()
	 */
	@Override
	public String getNumber() {
		return number;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.bean.RegisterI#setNumber(
	 * java.lang.String)
	 */
	@Override
	public void setNumber(String number) {
		this.number = number;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.bean.RegisterI#getDate()
	 */
	@Override
	public XMLGregorianCalendar getDate() {
		return date;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.bean.RegisterI#setDate(
	 * javax.xml.datatype.XMLGregorianCalendar)
	 */
	@Override
	public void setDate(XMLGregorianCalendar date) {
		this.date = date;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.bean.RegisterI#getUserName(
	 * )
	 */
	@Override
	public String getUserName() {
		return userName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.bean.RegisterI#setUserName(
	 * java.lang.String)
	 */
	@Override
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.bean.RegisterI#
	 * getSystemDate()
	 */
	@Override
	public XMLGregorianCalendar getSystemDate() {
		return systemDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.bean.RegisterI#
	 * setSystemDate(javax.xml.datatype.XMLGregorianCalendar)
	 */
	@Override
	public void setSystemDate(XMLGregorianCalendar systemDate) {
		this.systemDate = systemDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.bean.RegisterI#getOffice()
	 */
	@Override
	public String getOffice() {
		return office;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.bean.RegisterI#setOffice(
	 * java.lang.String)
	 */
	@Override
	public void setOffice(String office) {
		this.office = office;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.bean.RegisterI#
	 * getOfficeName()
	 */
	@Override
	public String getOfficeName() {
		return officeName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.registry.bean.RegisterI#
	 * setOfficeName(java.lang.String)
	 */
	@Override
	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.bean.RegisterI#getBookId()
	 */
	@Override
	public int getBookId() {
		return bookId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.bean.RegisterI#setBookId(
	 * int)
	 */
	@Override
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.bean.RegisterI#getFolderId(
	 * )
	 */
	@Override
	public int getFolderId() {
		return folderId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.bean.RegisterI#setFolderId(
	 * int)
	 */
	@Override
	public void setFolderId(int folderId) {
		this.folderId = folderId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.bean.RegisterI#getState()
	 */
	@Override
	public int getState() {
		return state;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.bean.RegisterI#setState(
	 * int)
	 */
	@Override
	public void setState(int state) {
		this.state = state;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.registry.bean.RegisterI#importData(
	 * java.lang.Object)
	 */
	@Override
	public void importData(Object o) {
		if (o instanceof WSRegister) {
			WSRegister wsRegister = (WSRegister) o;
			setNumber(wsRegister.getNumber());
			setDate(wsRegister.getDate());
			setUserName(wsRegister.getUserName());
			setSystemDate(wsRegister.getSystemDate());
			setOffice(wsRegister.getOffice());
			setOfficeName(wsRegister.getOfficeName());
			setBookId(wsRegister.getBookId());
			setFolderId(wsRegister.getFolderId());
			setState(wsRegister.getState());
		}

	}

}

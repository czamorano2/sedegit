
package es.imserso.sede.service.registration.registry.ISicres.books.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the es.imserso.sede.service.registration.registry.ISicres.books.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class BooksObjectFactory {

    private final static QName _Security_QNAME = new QName("http://schemas.xmlsoap.org/ws/2002/04/secext", "Security");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: es.imserso.sede.service.registration.registry.ISicres.books.client
     * 
     */
    public BooksObjectFactory() {
    }

    /**
     * Create an instance of {@link WSGetBookSchema }
     * 
     */
    public WSGetBookSchema createWSGetBookSchema() {
        return new WSGetBookSchema();
    }

    /**
     * Create an instance of {@link WSGetBookSchemaResponse }
     * 
     */
    public WSGetBookSchemaResponse createWSGetBookSchemaResponse() {
        return new WSGetBookSchemaResponse();
    }

    /**
     * Create an instance of {@link ArrayOfWSField }
     * 
     */
    public ArrayOfWSField createArrayOfWSField() {
        return new ArrayOfWSField();
    }

    /**
     * Create an instance of {@link WSGetInputBooks }
     * 
     */
    public WSGetInputBooks createWSGetInputBooks() {
        return new WSGetInputBooks();
    }

    /**
     * Create an instance of {@link WSGetInputBooksResponse }
     * 
     */
    public WSGetInputBooksResponse createWSGetInputBooksResponse() {
        return new WSGetInputBooksResponse();
    }

    /**
     * Create an instance of {@link ArrayOfWSBook }
     * 
     */
    public ArrayOfWSBook createArrayOfWSBook() {
        return new ArrayOfWSBook();
    }

    /**
     * Create an instance of {@link WSGetOutputBooks }
     * 
     */
    public WSGetOutputBooks createWSGetOutputBooks() {
        return new WSGetOutputBooks();
    }

    /**
     * Create an instance of {@link WSGetOutputBooksResponse }
     * 
     */
    public WSGetOutputBooksResponse createWSGetOutputBooksResponse() {
        return new WSGetOutputBooksResponse();
    }

    /**
     * Create an instance of {@link WSBook }
     * 
     */
    public WSBook createWSBook() {
        return new WSBook();
    }

    /**
     * Create an instance of {@link WSField }
     * 
     */
    public WSField createWSField() {
        return new WSField();
    }

    /**
     * Create an instance of {@link Security }
     * 
     */
    public Security createSecurity() {
        return new Security();
    }

    /**
     * Create an instance of {@link UsernameTokenClass }
     * 
     */
    public UsernameTokenClass createUsernameTokenClass() {
        return new UsernameTokenClass();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Security }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.xmlsoap.org/ws/2002/04/secext", name = "Security")
    public JAXBElement<Security> createSecurity(Security value) {
        return new JAXBElement<Security>(_Security_QNAME, Security.class, null, value);
    }

}

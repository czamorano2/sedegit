
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para WSParamOutputRegisterEx complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="WSParamOutputRegisterEx">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.invesicres.org}WSParamOutputRegister">
 *       &lt;sequence>
 *         &lt;element name="Documents" type="{http://www.invesicres.org}ArrayOfWSParamDocument" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSParamOutputRegisterEx", propOrder = {
    "documents"
})
public class WSParamOutputRegisterEx
    extends WSParamOutputRegister
{

    @XmlElement(name = "Documents")
    protected ArrayOfWSParamDocument documents;

    /**
     * Obtiene el valor de la propiedad documents.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSParamDocument }
     *     
     */
    public ArrayOfWSParamDocument getDocuments() {
        return documents;
    }

    /**
     * Define el valor de la propiedad documents.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSParamDocument }
     *     
     */
    public void setDocuments(ArrayOfWSParamDocument value) {
        this.documents = value;
    }

}

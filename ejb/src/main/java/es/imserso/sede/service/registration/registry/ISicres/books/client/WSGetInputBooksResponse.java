
package es.imserso.sede.service.registration.registry.ISicres.books.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="WSGetInputBooksResult" type="{http://www.invesicres.org}ArrayOfWSBook" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsGetInputBooksResult"
})
@XmlRootElement(name = "WSGetInputBooksResponse")
public class WSGetInputBooksResponse {

    @XmlElement(name = "WSGetInputBooksResult")
    protected ArrayOfWSBook wsGetInputBooksResult;

    /**
     * Obtiene el valor de la propiedad wsGetInputBooksResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSBook }
     *     
     */
    public ArrayOfWSBook getWSGetInputBooksResult() {
        return wsGetInputBooksResult;
    }

    /**
     * Define el valor de la propiedad wsGetInputBooksResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSBook }
     *     
     */
    public void setWSGetInputBooksResult(ArrayOfWSBook value) {
        this.wsGetInputBooksResult = value;
    }

}

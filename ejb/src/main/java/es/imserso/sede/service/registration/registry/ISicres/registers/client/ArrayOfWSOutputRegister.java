
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfWSOutputRegister complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWSOutputRegister">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WSOutputRegister" type="{http://www.invesicres.org}WSOutputRegister" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWSOutputRegister", propOrder = {
    "wsOutputRegister"
})
public class ArrayOfWSOutputRegister {

    @XmlElement(name = "WSOutputRegister", nillable = true)
    protected List<WSOutputRegister> wsOutputRegister;

    /**
     * Gets the value of the wsOutputRegister property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wsOutputRegister property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWSOutputRegister().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WSOutputRegister }
     * 
     * 
     */
    public List<WSOutputRegister> getWSOutputRegister() {
        if (wsOutputRegister == null) {
            wsOutputRegister = new ArrayList<WSOutputRegister>();
        }
        return this.wsOutputRegister;
    }

}

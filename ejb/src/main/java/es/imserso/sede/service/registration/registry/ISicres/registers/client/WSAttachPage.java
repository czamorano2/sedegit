
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BookIdentification" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RegisterIdentification" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Documents" type="{http://www.invesicres.org}ArrayOfWSParamDocument" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bookIdentification",
    "registerIdentification",
    "documents"
})
@XmlRootElement(name = "WSAttachPage")
public class WSAttachPage {

    @XmlElement(name = "BookIdentification")
    protected int bookIdentification;
    @XmlElement(name = "RegisterIdentification")
    protected int registerIdentification;
    @XmlElement(name = "Documents")
    protected ArrayOfWSParamDocument documents;

    /**
     * Obtiene el valor de la propiedad bookIdentification.
     * 
     */
    public int getBookIdentification() {
        return bookIdentification;
    }

    /**
     * Define el valor de la propiedad bookIdentification.
     * 
     */
    public void setBookIdentification(int value) {
        this.bookIdentification = value;
    }

    /**
     * Obtiene el valor de la propiedad registerIdentification.
     * 
     */
    public int getRegisterIdentification() {
        return registerIdentification;
    }

    /**
     * Define el valor de la propiedad registerIdentification.
     * 
     */
    public void setRegisterIdentification(int value) {
        this.registerIdentification = value;
    }

    /**
     * Obtiene el valor de la propiedad documents.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSParamDocument }
     *     
     */
    public ArrayOfWSParamDocument getDocuments() {
        return documents;
    }

    /**
     * Define el valor de la propiedad documents.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSParamDocument }
     *     
     */
    public void setDocuments(ArrayOfWSParamDocument value) {
        this.documents = value;
    }

}

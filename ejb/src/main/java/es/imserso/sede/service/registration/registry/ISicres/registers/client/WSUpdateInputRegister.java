
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BookIdentification" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RegisterIdentification" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Fields" type="{http://www.invesicres.org}ArrayOfWSParamField" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bookIdentification",
    "registerIdentification",
    "fields"
})
@XmlRootElement(name = "WSUpdateInputRegister")
public class WSUpdateInputRegister {

    @XmlElement(name = "BookIdentification")
    protected int bookIdentification;
    @XmlElement(name = "RegisterIdentification")
    protected int registerIdentification;
    @XmlElement(name = "Fields")
    protected ArrayOfWSParamField fields;

    /**
     * Obtiene el valor de la propiedad bookIdentification.
     * 
     */
    public int getBookIdentification() {
        return bookIdentification;
    }

    /**
     * Define el valor de la propiedad bookIdentification.
     * 
     */
    public void setBookIdentification(int value) {
        this.bookIdentification = value;
    }

    /**
     * Obtiene el valor de la propiedad registerIdentification.
     * 
     */
    public int getRegisterIdentification() {
        return registerIdentification;
    }

    /**
     * Define el valor de la propiedad registerIdentification.
     * 
     */
    public void setRegisterIdentification(int value) {
        this.registerIdentification = value;
    }

    /**
     * Obtiene el valor de la propiedad fields.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSParamField }
     *     
     */
    public ArrayOfWSParamField getFields() {
        return fields;
    }

    /**
     * Define el valor de la propiedad fields.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSParamField }
     *     
     */
    public void setFields(ArrayOfWSParamField value) {
        this.fields = value;
    }

}

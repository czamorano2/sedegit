
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WSLoadOutputRegistersResult" type="{http://www.invesicres.org}WSOutputRegistersResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsLoadOutputRegistersResult"
})
@XmlRootElement(name = "WSLoadOutputRegistersResponse")
public class WSLoadOutputRegistersResponse {

    @XmlElement(name = "WSLoadOutputRegistersResult")
    protected WSOutputRegistersResponse wsLoadOutputRegistersResult;

    /**
     * Obtiene el valor de la propiedad wsLoadOutputRegistersResult.
     * 
     * @return
     *     possible object is
     *     {@link WSOutputRegistersResponse }
     *     
     */
    public WSOutputRegistersResponse getWSLoadOutputRegistersResult() {
        return wsLoadOutputRegistersResult;
    }

    /**
     * Define el valor de la propiedad wsLoadOutputRegistersResult.
     * 
     * @param value
     *     allowed object is
     *     {@link WSOutputRegistersResponse }
     *     
     */
    public void setWSLoadOutputRegistersResult(WSOutputRegistersResponse value) {
        this.wsLoadOutputRegistersResult = value;
    }

}


package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WSImportOutputRegisterResult" type="{http://www.invesicres.org}WSRegister" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsImportOutputRegisterResult"
})
@XmlRootElement(name = "WSImportOutputRegisterResponse")
public class WSImportOutputRegisterResponse {

    @XmlElement(name = "WSImportOutputRegisterResult")
    protected WSRegister wsImportOutputRegisterResult;

    /**
     * Obtiene el valor de la propiedad wsImportOutputRegisterResult.
     * 
     * @return
     *     possible object is
     *     {@link WSRegister }
     *     
     */
    public WSRegister getWSImportOutputRegisterResult() {
        return wsImportOutputRegisterResult;
    }

    /**
     * Define el valor de la propiedad wsImportOutputRegisterResult.
     * 
     * @param value
     *     allowed object is
     *     {@link WSRegister }
     *     
     */
    public void setWSImportOutputRegisterResult(WSRegister value) {
        this.wsImportOutputRegisterResult = value;
    }

}

package es.imserso.sede.service.registration.registry.ISicres.books.client.clientsample;

import java.util.ArrayList;
import java.util.List;

import es.imserso.sede.service.registration.registry.ISicres.books.bean.Book;
import es.imserso.sede.service.registration.registry.ISicres.books.client.ISWebServiceBooksSoap;
import es.imserso.sede.service.registration.registry.ISicres.books.client.ISWebServiceBooksSoapImplService;
import es.imserso.sede.service.registration.registry.ISicres.books.client.Security;
import es.imserso.sede.service.registration.registry.ISicres.books.client.UsernameTokenClass;
import es.imserso.sede.service.registration.registry.ISicres.books.client.WSBook;
import es.imserso.sede.service.registration.registry.ISicres.books.client.WSGetInputBooks;
import es.imserso.sede.service.registration.registry.ISicres.books.client.WSGetInputBooksResponse;
import es.imserso.sede.service.registration.registry.bean.BookI;

public class ClientSample {

	public static void main(String[] args) {
	        try {
				System.out.println("***********************");
				System.out.println("Create Web Service Client...");
				ISWebServiceBooksSoapImplService service1 = new ISWebServiceBooksSoapImplService();
				System.out.println("Create Web Service...");
				ISWebServiceBooksSoap port1 = service1.getISWebServiceBooksSoapImplPort();
//	        System.out.println("Call Web Service Operation...");
//	        System.out.println("Server said: " + port1.wsGetBookSchema(null,null));
//	        //Please input the parameters instead of 'null' for the upper method!
//	
//	        System.out.println("Server said: " + port1.wsGetOutputBooks(null,null));
//	        //Please input the parameters instead of 'null' for the upper method!
				
				UsernameTokenClass usernameToken = new UsernameTokenClass();
				usernameToken.setUsername("srvwsinvesicres");
				usernameToken.setPassword("Imserso2014");
				Security security = new Security();
				security.setUsernameToken(usernameToken);
				
				WSGetInputBooks parameters = new WSGetInputBooks();
				WSGetInputBooksResponse wsGetInputBooksResponse = port1.wsGetInputBooks(parameters, security);
				System.out.println("se ha obtenido la información de los libros  del registro");

				List<BookI> bookList = new ArrayList<BookI>();
				for (WSBook book : wsGetInputBooksResponse.getWSGetInputBooksResult().getWSBook()) {
					Book b = new Book();
					b.setId(book.getId());
					b.setName(book.getName());
					b.setType(book.getType());
					bookList.add(b);
				}
				System.out.println("Server said: " + port1.wsGetInputBooks(parameters, security));
				//Please input the parameters instead of 'null' for the upper method!

				System.out.println("Create Web Service...");
				ISWebServiceBooksSoap port2 = service1.getISWebServiceBooksSoapImplPort();
				System.out.println("Call Web Service Operation...");
				System.out.println("Server said: " + port2.wsGetBookSchema(null,null));
				//Please input the parameters instead of 'null' for the upper method!

				System.out.println("Server said: " + port2.wsGetOutputBooks(null,null));
				//Please input the parameters instead of 'null' for the upper method!

				System.out.println("Server said: " + port2.wsGetInputBooks(null,null));
				//Please input the parameters instead of 'null' for the upper method!

				System.out.println("***********************");
				System.out.println("Call Over!");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
}

package es.imserso.sede.service.registration.receipt;

import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import es.imserso.sede.config.PropertyComponent;
import es.imserso.sede.util.Utils;

/**
 * Implementación de la interfaz que ofrece las funcionalidades relacionadas con
 * los justificantes.
 * 
 * @author 11825775
 *
 */
public class ReceiptServiceImpl implements ReceiptServiceI {

	@Inject
	private Logger log;

	@Inject
	PropertyComponent propertyComponent;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.service.registration.receipt.ReceiptServiceI#signReceipt(
	 * byte[])
	 */
	@Override
	public byte[] signReceipt(byte[] receipt) throws ReceiptServiceException {
		log.info("firmando justificante...");

		byte[] result = null;
		InputStream inputStream = null;
		int responseCode;

		try {
			// Using the RESTEasy libraries, initiate a client request
			ResteasyClient client = new ResteasyClientBuilder().build();
			// Set url as target
			log.debug(
					"conectamos al servicio de firma... (" + propertyComponent.getUrlDocumentSignatureService() + ")");
			ResteasyWebTarget target = client.target(propertyComponent.getUrlDocumentSignatureService());

			log.debug("enviamos el documento a firmar...");
			Response response = target.request().post(Entity.entity(receipt, MediaType.APPLICATION_OCTET_STREAM));

			log.debug("obtenemos el código de respuesta...");
			responseCode = response.getStatus();
			log.info("código de respuesta: " + responseCode);

			if (response.getStatus() != 200) {
				String errmsg = "Error con código HTTP: " + responseCode;
				log.warn(errmsg);
				throw new ReceiptServiceException(errmsg);
			}

			log.info("ResponseMessageFromServer: " + response.getStatusInfo().getReasonPhrase());

			log.debug("leemos el array de bytes del response...");
			inputStream = response.readEntity(InputStream.class);

			log.debug("leemos el inputStream que nos ha devuelto el servicio REST de la firma...");
			result = Utils.getBytes(inputStream);

			log.info("justificante firmado!");

		} catch (IllegalArgumentException | NullPointerException | IOException e) {
			String errmsg = "error al firmar el documento: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new ReceiptServiceException(errmsg);

		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.warn("error al cerrar el inputStream: " + Utils.getExceptionMessage(e));
				}
			}
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.imserso.sede.service.registration.receipt.ReceiptServiceI#isAlive()
	 */
	@Override
	public Boolean isAlive() throws ReceiptServiceException {
		log.info("comprobando si están activos los servicios de firma electrónica...");

		Boolean result = Boolean.FALSE;
		int responseCode;
		ResteasyClient client = null;

		try {
			client = new ResteasyClientBuilder().build();

			String url = propertyComponent.getUrlDocumentSignatureIsAlive();
			log.debug(url);
			ResteasyWebTarget target = client.target(url);

			log.debug("enviamos la petición...");
			Response response = target.request().get();

			log.debug("obtenemos el código de respuesta...");
			responseCode = response.getStatus();
			log.info("código de respuesta: " + responseCode);

			if (!(result = ((response.getStatus()) >=  200 && (response.getStatus() <300)))) {
				String errmsg = "Error con código HTTP: " + responseCode;
				log.warn(errmsg);
				throw new ReceiptServiceException(errmsg);
			}

			log.info("ResponseMessageFromServer: " + response.getStatusInfo().getReasonPhrase());

		} catch (Exception e) {
			String errmsg = "error al comprobar si están activos los servicios de firma electrónica: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new ReceiptServiceException(errmsg);

		} finally {
			if (client != null) {
				try {
					client.close();
				} catch (Exception e) {
					String errmsg = "error al cerrar el cliente REST: " + Utils.getExceptionMessage(e);
					log.error(errmsg);
					throw new ReceiptServiceException(errmsg);
				}
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see es.imserso.sede.service.registration.receipt.ReceiptServiceI#testServices()
	 */
	@Override
	public Boolean testServices() throws ReceiptServiceException {
		log.info("comprobando si funcionan los servicios de firma electrónica...");

		Boolean result = Boolean.FALSE;
		int responseCode;
		ResteasyClient client = null;

		try {
			client = new ResteasyClientBuilder().build();

			String url = propertyComponent.getUrlDocumentSignatureServicesTest();
			log.debug(url);
			ResteasyWebTarget target = client.target(url);

			log.debug("enviamos la petición...");
			Response response = target.request().get();

			log.debug("obtenemos el código de respuesta...");
			responseCode = response.getStatus();


			if (!(result = ((response.getStatus()) >=  200 && (response.getStatus() <300)))) {
				String errmsg = "Error con código HTTP: " + responseCode;
				log.warn(errmsg);
				throw new ReceiptServiceException(errmsg);
			}

			log.info("ResponseMessageFromServer: " + response.getStatusInfo().getReasonPhrase());

		} catch (Exception e) {
			String errmsg = "error al comprobar si funcionan los servicios de firma electrónica: " + Utils.getExceptionMessage(e);
			log.error(errmsg);
			throw new ReceiptServiceException(errmsg);

		} finally {
			if (client != null) {
				try {
					client.close();
				} catch (Exception e) {
					String errmsg = "error al cerrar el cliente REST: " + Utils.getExceptionMessage(e);
					log.error(errmsg);
					throw new ReceiptServiceException(errmsg);
				}
			}
		}
		return result;
	}

}

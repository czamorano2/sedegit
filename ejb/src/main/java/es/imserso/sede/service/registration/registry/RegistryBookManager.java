package es.imserso.sede.service.registration.registry;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.constraints.Pattern;

import org.jboss.logging.Logger;

import es.imserso.sede.service.registration.RegistrationException;
import es.imserso.sede.service.registration.registry.ISicres.books.BookRegistryServiceI;
import es.imserso.sede.service.registration.registry.bean.BookI;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.exception.SedeRuntimeException;

/**
 * Gestor de libros de registro en InveSicres.
 * <p>
 * El HashMap tendrá como claves todos los años de los libros de entrada de la
 * sede en InveSicres.
 * <p>
 * Si al HashMap se le pide una clave que no tiene, se actualizará invocando al
 * servicio web que lista los libros del usuario.
 * 
 * @author 11825775
 *
 */
@ApplicationScoped
public class RegistryBookManager {

	@Inject
	private Logger log;

	@Inject
	private BookRegistryServiceI bookRegistryService;

	/**
	 * Contiene información de los libros de entrada de la sede (la clave es el año)
	 */
	private HashMap<String, BookI> bookMap;

	/**
	 * @param inputDate
	 *            fecha en la que se grabó el registro
	 * @return libro en el que se grabó el registro
	 * @throws SedeException
	 */
	public BookI getBook(Date inputDate) throws SedeException {
		Calendar cal = new GregorianCalendar();
		cal.setTime(inputDate);
		return getBook(cal.get(Calendar.YEAR));
	}

	/**
	 * @param year
	 *            año en el que se grabó el registro
	 * @return libro en el que se grabó el registro
	 * @throws NumberFormatException
	 * @throws SedeException
	 */
	public BookI getBook(@Pattern(regexp = "^(20)\\d{2}$", message = "el año debe tener 4 dígitos (20XX)") String year)
			throws NumberFormatException, SedeException {
		return getBook(Integer.valueOf(year));
	}

	/**
	 * @param year
	 *            año del libro de entrada (formato: yyyy)
	 * @return objeto con la información del libro del registro
	 * @throws SedeException
	 */
	public BookI getBook(int year) throws SedeRuntimeException {
		if (bookMap == null) {
			// actualizamos el HashMap
			updateBookMap();
		}

		BookI book = null;
		if (bookMap != null) {
			book = bookMap.get(String.valueOf(year));
			if (book == null) {
				// actualizamos el HashMap
				updateBookMap();

				book = bookMap.get(RegistryUtils.getBookName(year));
				if (book == null) {
					throw new SedeRuntimeException(
							String.format("No se ha encontrado el libro del año %d en el registro!", year));
				}
			}
		}

		return book;
	}

	/**
	 * @return libros de entrada de la sede electrónica
	 * @throws RegistrationException
	 */
	public List<BookI> getSedeInputBooks() throws RegistrationException {
		return bookRegistryService.getInputBooks().stream().filter(l -> l.getName().startsWith("sede20"))
				.collect(Collectors.toList());
	}

	/**
	 * Actualiza el HashMap invocando al servicio web que lista los libros del
	 * usuario.
	 * 
	 * @throws RegistrationException
	 */
	private void updateBookMap() throws RegistrationException {
		log.debug("actualizando colección de libros del registro");

		List<BookI> bookList = getSedeInputBooks();
		bookMap = new HashMap<String, BookI>();
		for (BookI book : bookList) {
			bookMap.put(book.getName(), book);
			log.debug(String.format("se añade el libro de entrada %s", book.getName()));
		}

		log.info(String.format("colección de libros del registro actualizada con %d elementos", bookMap.size()));
	}

	/**
	 * @return año del libro actual
	 */
	public int getCurrentBookYear() {
		return LocalDate.now().getYear();
	}

	/**
	 * @return nombre del libro actual
	 * @throws SedeException
	 */
	public String getCurrentBookName() throws SedeException {
		return getCurrentBook().getName();
	}

	/**
	 * @return libro del año actual
	 * @throws SedeException
	 */
	public BookI getCurrentBook() throws SedeRuntimeException {
		return getBook(LocalDate.now().getYear());
	}

}


package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para WSParamOutputRegister complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="WSParamOutputRegister">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Sender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Destination" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransportType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransportNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MatterType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Matter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Persons" type="{http://www.invesicres.org}ArrayOfWSParamPerson" minOccurs="0"/>
 *         &lt;element name="AddFields" type="{http://www.invesicres.org}ArrayOfWSAddField" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSParamOutputRegister", propOrder = {
    "sender",
    "destination",
    "transportType",
    "transportNumber",
    "matterType",
    "matter",
    "persons",
    "addFields"
})
@XmlSeeAlso({
    WSParamOutputRegisterEx.class,
    WSParamInputRegister.class
})
public class WSParamOutputRegister {

    @XmlElement(name = "Sender")
    protected String sender;
    @XmlElement(name = "Destination")
    protected String destination;
    @XmlElement(name = "TransportType")
    protected String transportType;
    @XmlElement(name = "TransportNumber")
    protected String transportNumber;
    @XmlElement(name = "MatterType")
    protected String matterType;
    @XmlElement(name = "Matter")
    protected String matter;
    @XmlElement(name = "Persons")
    protected ArrayOfWSParamPerson persons;
    @XmlElement(name = "AddFields")
    protected ArrayOfWSAddField addFields;

    /**
     * Obtiene el valor de la propiedad sender.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSender() {
        return sender;
    }

    /**
     * Define el valor de la propiedad sender.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSender(String value) {
        this.sender = value;
    }

    /**
     * Obtiene el valor de la propiedad destination.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Define el valor de la propiedad destination.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestination(String value) {
        this.destination = value;
    }

    /**
     * Obtiene el valor de la propiedad transportType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransportType() {
        return transportType;
    }

    /**
     * Define el valor de la propiedad transportType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransportType(String value) {
        this.transportType = value;
    }

    /**
     * Obtiene el valor de la propiedad transportNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransportNumber() {
        return transportNumber;
    }

    /**
     * Define el valor de la propiedad transportNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransportNumber(String value) {
        this.transportNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad matterType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatterType() {
        return matterType;
    }

    /**
     * Define el valor de la propiedad matterType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatterType(String value) {
        this.matterType = value;
    }

    /**
     * Obtiene el valor de la propiedad matter.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatter() {
        return matter;
    }

    /**
     * Define el valor de la propiedad matter.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatter(String value) {
        this.matter = value;
    }

    /**
     * Obtiene el valor de la propiedad persons.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSParamPerson }
     *     
     */
    public ArrayOfWSParamPerson getPersons() {
        return persons;
    }

    /**
     * Define el valor de la propiedad persons.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSParamPerson }
     *     
     */
    public void setPersons(ArrayOfWSParamPerson value) {
        this.persons = value;
    }

    /**
     * Obtiene el valor de la propiedad addFields.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSAddField }
     *     
     */
    public ArrayOfWSAddField getAddFields() {
        return addFields;
    }

    /**
     * Define el valor de la propiedad addFields.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSAddField }
     *     
     */
    public void setAddFields(ArrayOfWSAddField value) {
        this.addFields = value;
    }

}

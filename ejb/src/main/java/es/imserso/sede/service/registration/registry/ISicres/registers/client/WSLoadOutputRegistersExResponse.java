
package es.imserso.sede.service.registration.registry.ISicres.registers.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WSLoadOutputRegistersExResult" type="{http://www.invesicres.org}WSOutputRegistersResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsLoadOutputRegistersExResult"
})
@XmlRootElement(name = "WSLoadOutputRegistersExResponse")
public class WSLoadOutputRegistersExResponse {

    @XmlElement(name = "WSLoadOutputRegistersExResult")
    protected WSOutputRegistersResponse wsLoadOutputRegistersExResult;

    /**
     * Obtiene el valor de la propiedad wsLoadOutputRegistersExResult.
     * 
     * @return
     *     possible object is
     *     {@link WSOutputRegistersResponse }
     *     
     */
    public WSOutputRegistersResponse getWSLoadOutputRegistersExResult() {
        return wsLoadOutputRegistersExResult;
    }

    /**
     * Define el valor de la propiedad wsLoadOutputRegistersExResult.
     * 
     * @param value
     *     allowed object is
     *     {@link WSOutputRegistersResponse }
     *     
     */
    public void setWSLoadOutputRegistersExResult(WSOutputRegistersResponse value) {
        this.wsLoadOutputRegistersExResult = value;
    }

}

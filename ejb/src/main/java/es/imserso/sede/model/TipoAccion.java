package es.imserso.sede.model;

/**
 * Acciones que se pueden realizar sobre un trámite
 * 
 * @author 11825775
 *
 */
public enum TipoAccion {
	alta, edicion, consulta
}

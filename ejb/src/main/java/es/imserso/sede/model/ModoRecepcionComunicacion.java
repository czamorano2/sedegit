package es.imserso.sede.model;

import javax.validation.constraints.NotNull;

public enum ModoRecepcionComunicacion {
	VIA_ELECTRONICA("VE", "Vía Electróniva"), CORREO_POSTAL("CP", "Correo Postal");

	@NotNull
	private String codigo;

	@NotNull
	private String descripcion;

	private ModoRecepcionComunicacion(@NotNull String _codigo, @NotNull String _descripcion) {
		this.codigo = _codigo;
		this.descripcion = _descripcion;
	}
	
	public String codigo() {
		return codigo;
	}
	
	public String descripcion() {
		return descripcion;
	}

}

/*
 * ============================================================================
 *                   GNU Lesser General Public License
 * ============================================================================
 *
 * Taylor - The Java Enterprise Application Framework.
 * Copyright (C) 2005 John Gilbert jgilbert01@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 * John Gilbert
 * Email: jgilbert01@users.sourceforge.net
 */
package es.imserso.sede.model;

import static javax.persistence.LockModeType.READ;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Tramite
 *
 * @author 11825775
 * @generated
 */
@Entity
@SequenceGenerator(name = "SEQ_TRAMITE", initialValue = 1, allocationSize = 1, sequenceName = "SEC_TRAMITE")
@XmlRootElement
@NamedQuery(name = "all", query = "select t from Tramite t", lockMode = READ)
public class Tramite implements Serializable {

	private static final long serialVersionUID = -2624343237209875421L;

	/** @generated */
	public Tramite() {
	}

	/**
	 * ------------------------------------------ The primary key.
	 *
	 * @generated
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TRAMITE")
	public Long getId() {
		return id;
	}

	/** @generated */
	public void setId(final Long id) {
		this.id = id;
	}

	/** @generated */
	private Long id = null;

	/**
	 * ------------------------------------------ codigo SIA del tramite
	 *
	 * @generated
	 */
	@NotNull(message = "el codigo SIA es obligatorio")
	@Column(unique = true, length = 10, nullable = false)
	public String getCodigoSIA() {
		return codigoSIA;
	}

	/** @generated */
	public void setCodigoSIA(final String codigoSIA) {
		this.codigoSIA = codigoSIA;
	}

	/** @generated */
	private String codigoSIA = null;

	/**
	 * ------------------------------------------ descripcion del tramite
	 *
	 * @generated
	 */
	@NotNull(message = "la descripcion es obligatoria")
	public String getDescripcion() {
		return descripcion;
	}

	/** @generated */
	public void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}

	/** @generated */
	private String descripcion = null;

	/**
	 * ------------------------------------------ nombre del tramite
	 *
	 * @generated
	 */
	@NotNull(message = "el nombre es obligatorio")
	public String getNombre() {
		return nombre;
	}

	/** @generated */
	public void setNombre(final String nombre) {
		this.nombre = nombre;
	}

	/** @generated */
	private String nombre = null;

	/**
	 * ------------------------------------------ solicitudes asociadas al tramite
	 *
	 * @generated
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "tramite", cascade = CascadeType.MERGE, orphanRemoval = false)
	public List<Solicitud> getSolicitudes() {
		if (this.solicitudes == null) {
			this.solicitudes = new ArrayList<Solicitud>();
		}
		return solicitudes;
	}

	/** @generated */
	public void setSolicitudes(final List<Solicitud> solicitudes) {
		this.solicitudes = solicitudes;
	}

	/**
	 * Associate Tramite with Solicitud
	 * 
	 * @generated
	 */
	public void addSolicitude(Solicitud solicitude) {
		if (solicitude == null) {
			return;
		}
		getSolicitudes().add(solicitude);
		solicitude.setTramite(this);
	}

	/**
	 * Unassociate Tramite from Solicitud
	 * 
	 * @generated
	 */
	public void removeSolicitude(Solicitud solicitude) {
		if (solicitude == null) {
			return;
		}
		getSolicitudes().remove(solicitude);
		solicitude.setTramite(null);
	}

	/**
	 * @generated
	 */
	public void removeAllSolicitudes() {
		List<Solicitud> remove = new ArrayList<Solicitud>();
		remove.addAll(getSolicitudes());
		for (Solicitud element : remove) {
			removeSolicitude(element);
		}
	}

	/** @generated */
	private List<Solicitud> solicitudes = null;

	/**
	 * ------------------------------------------ usuarios que pueden gestionar el
	 * tramite
	 *
	 * @generated
	 */
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "tramites", cascade = CascadeType.MERGE)
	public Set<Usuario> getUsuarios() {
		if (this.usuarios == null) {
			this.usuarios = new HashSet<Usuario>();
		}
		return usuarios;
	}

	/** @generated */
	public void setUsuarios(final Set<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	/**
	 * Associate Tramite with Usuario
	 * 
	 * @generated
	 */
	public void addUsuario(Usuario usuario) {
		if (usuario == null) {
			return;
		}
		getUsuarios().add(usuario);
		usuario.getTramites().add(this);
	}

	/**
	 * Unassociate Tramite from Usuario
	 * 
	 * @generated
	 */
	public void removeUsuario(Usuario usuario) {
		if (usuario == null) {
			return;
		}
		getUsuarios().remove(usuario);
		usuario.getTramites().remove(this);
	}

	/**
	 * @generated
	 */
	public void removeAllUsuarios() {
		List<Usuario> remove = new ArrayList<Usuario>();
		remove.addAll(getUsuarios());
		for (Usuario element : remove) {
			removeUsuario(element);
		}
	}

	/** @generated */
	private Set<Usuario> usuarios = null;

	/**
	 * ------------------------------------------ The optimistic lock property.
	 *
	 * @generated
	 */
	@Version
	public Long getOplock() {
		return oplock;
	}

	/** @generated */
	public void setOplock(final Long oplock) {
		this.oplock = oplock;
	}

	/** @generated */
	private Long oplock = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@ManyToOne(optional = true, fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	public JustificantePdf getJustificantePdf() {
		return justificantePdf;
	}

	/** @generated */
	public void setJustificantePdf(final JustificantePdf justificantePdf) {
		this.justificantePdf = justificantePdf;
	}

	/** @generated */
	private JustificantePdf justificantePdf = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@ManyToOne(optional = true, fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	public SolicitudPdf getSolicitudPdf() {
		return solicitudPdf;
	}

	/** @generated */
	public void setSolicitudPdf(final SolicitudPdf solicitudPdf) {
		this.solicitudPdf = solicitudPdf;
	}

	/** @generated */
	private SolicitudPdf solicitudPdf = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@NotNull(message = "el campo unidad responsable es obligatorio")
	@Length(min = 1, max = 75, message = "el valor del campo unidad responsable debe tener entre 1 y 75 caracteres")
	public String getUnidadResponsable() {
		return unidadResponsable;
	}

	/** @generated */
	public void setUnidadResponsable(final String unidadResponsable) {
		this.unidadResponsable = unidadResponsable;
	}

	/** @generated */
	private String unidadResponsable = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	public String getClaveUCM() {
		return claveUCM;
	}

	/** @generated */
	public void setClaveUCM(final String claveUCM) {
		this.claveUCM = claveUCM;
	}

	/** @generated */
	private String claveUCM = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@ManyToOne(optional = true, fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	public PlantillaEmail getPlantillaEmail() {
		return plantillaEmail;
	}

	/** @generated */
	public void setPlantillaEmail(final PlantillaEmail plantillaEmail) {
		this.plantillaEmail = plantillaEmail;
	}

	/** @generated */
	private PlantillaEmail plantillaEmail = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	public String getObservacionesJustificante() {
		return observacionesJustificante;
	}

	/** @generated */
	public void setObservacionesJustificante(final String observacionesJustificante) {
		this.observacionesJustificante = observacionesJustificante;
	}

	/** @generated */
	private String observacionesJustificante = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "tramite", cascade = CascadeType.MERGE, orphanRemoval = true)
	public List<RemesaDescarga> getRemesasDescarga() {
		if (this.remesasDescarga == null) {
			this.remesasDescarga = new ArrayList<RemesaDescarga>();
		}
		return remesasDescarga;
	}

	/** @generated */
	public void setRemesasDescarga(final List<RemesaDescarga> remesasDescarga) {
		this.remesasDescarga = remesasDescarga;
	}

	/**
	 * Associate Tramite with RemesaDescarga
	 * 
	 * @generated
	 */
	public void addRemesasDescarga(RemesaDescarga remesasDescarga) {
		if (remesasDescarga == null) {
			return;
		}
		getRemesasDescarga().add(remesasDescarga);
		remesasDescarga.setTramite(this);
	}

	/**
	 * Unassociate Tramite from RemesaDescarga
	 * 
	 * @generated
	 */
	public void removeRemesasDescarga(RemesaDescarga remesasDescarga) {
		if (remesasDescarga == null) {
			return;
		}
		getRemesasDescarga().remove(remesasDescarga);
		remesasDescarga.setTramite(null);
	}

	/**
	 * @generated
	 */
	public void removeAllRemesasDescarga() {
		List<RemesaDescarga> remove = new ArrayList<RemesaDescarga>();
		remove.addAll(getRemesasDescarga());
		for (RemesaDescarga element : remove) {
			removeRemesasDescarga(element);
		}
	}

	/** @generated */
	private List<RemesaDescarga> remesasDescarga = null;

	@Transient
	public boolean isTurismo() {
		return this.codigoSIA.equalsIgnoreCase(TipoTramite.TURISMO.getSia());
	}

	@Transient
	public boolean isTermalismo() {
		return this.codigoSIA.equalsIgnoreCase(TipoTramite.TERMALISMO.getSia());
	}

	// ------------------------------------------
	// Utils
	// ------------------------------------------

	/** @NOT generated */
	public String toString() {
		return getNombre() + " " + getDescripcion();
	}

	/** @generated */
	@PreRemove
	public void preRemove() {
		removeAllUsuarios();
	}

	/** @generated */
	public Tramite deepClone() throws Exception {
		Tramite clone = (Tramite) super.clone();
		clone.setId(null);

		clone.setSolicitudes(null);
		for (Solicitud kid : this.getSolicitudes()) {
			clone.addSolicitude(kid.deepClone());
		}

		clone.setRemesasDescarga(null);
		for (RemesaDescarga kid : this.getRemesasDescarga()) {
			clone.addRemesasDescarga(kid.deepClone());
		}

		clone.setUsuarios(null);
		for (Usuario kid : this.getUsuarios()) {
			clone.addUsuario(kid);
		}
		return clone;
	}

	/** @generated */
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((id == null) ? super.hashCode() : id.hashCode());
		return result;
	}

	/** @generated */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Tramite))
			return false;
		final Tramite other = (Tramite) obj;
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		return true;
	}
}

/*
 * ============================================================================
 *                   GNU Lesser General Public License
 * ============================================================================
 *
 * Taylor - The Java Enterprise Application Framework.
 * Copyright (C) 2005 John Gilbert jgilbert01@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 * John Gilbert
 * Email: jgilbert01@users.sourceforge.net
 */
package es.imserso.sede.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

/**
 * Entidad que representa una plantilla para generar un documento PDF
 *
 * @author 11825775
 *
 */
@Entity
@SequenceGenerator(name = "SEQ_PLANTILLA_PDF", initialValue = 1, allocationSize = 1, sequenceName = "SEC_PLANTILLA_PDF")
@Table(uniqueConstraints = { @UniqueConstraint(name = "IDX_NOMBRE_PLANTILLA", columnNames = { "nombre"}) })
@Inheritance(strategy = InheritanceType.JOINED)
public class PlantillaPdf implements Serializable {

	private static final long serialVersionUID = -1291087444921924197L;

	public PlantillaPdf() {
	}

	/**
	 * ------------------------------------------ The primary key.
	 *
	 *
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PLANTILLA_PDF")
	public Long getId() {
		return id;
	}


	public void setId(final Long id) {
		this.id = id;
	}


	private Long id = null;

	/**
	 * ------------------------------------------ array de bytes que conforman
	 * la plantilla
	 *
	 *
	 */
	@NotNull
	public byte[] getValor() {
		return valor;
	}


	public void setValor(final byte[] valor) {
		this.valor = valor;
	}


	private byte[] valor = null;

	/**
	 * ------------------------------------------ nombre de la plantilla
	 *
	 *
	 */
	@NotNull
	public String getNombre() {
		return nombre;
	}


	public void setNombre(final String nombre) {
		this.nombre = nombre;
	}


	private String nombre = null;

	/**
	 * ------------------------------------------ descripcion de la plantilla
	 *
	 *
	 */
	@NotNull(message = "la descripcion es obligatoria")
	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}


	private String descripcion = null;

	/**
	 * ------------------------------------------ The optimistic lock property.
	 *
	 *
	 */
	@Version
	public Long getOplock() {
		return oplock;
	}


	public void setOplock(final Long oplock) {
		this.oplock = oplock;
	}


	private Long oplock = null;

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 *
	 */
	@ManyToOne(optional = true, fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	public Idioma getIdioma() {
		return idioma;
	}


	public void setIdioma(final Idioma idioma) {
		this.idioma = idioma;
	}


	private Idioma idioma = null;

	// ------------------------------------------
	// Utils
	// ------------------------------------------

	/** @NOT generated */
	public String toString() {
		return getNombre();
	}


	public PlantillaPdf deepClone() throws Exception {
		PlantillaPdf clone = (PlantillaPdf) super.clone();
		clone.setId(null);
		return clone;
	}


	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((id == null) ? super.hashCode() : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof PlantillaPdf))
			return false;
		final PlantillaPdf other = (PlantillaPdf) obj;
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		return true;
	}
}

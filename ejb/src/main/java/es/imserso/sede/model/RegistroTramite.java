package es.imserso.sede.model;

import static javax.persistence.EnumType.STRING;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import es.imserso.sede.service.registration.event.RegistrationPhase;
import es.imserso.sede.service.registration.event.RegistrationResult.Result;

/**
 * Entity implementation class for Entity: RegistroTramite
 *
 */

@NamedQuery(name = "allRegistrosTramites", query = "select rt from RegistroTramite rt order by rt.id desc")
@Entity
@SequenceGenerator(name = "SEQ_REGISTRO_TRAMITE", initialValue = 1, allocationSize = 1, sequenceName = "SEC_REGISTRO_TRAMITE")
public class RegistroTramite implements Serializable, Cloneable {

	private static final long serialVersionUID = -1598758514645581118L;

	private Long id;

	private String usuario;

	private Date inicio;

	private Date fin;

	private String sia;

	private TipoAccion accion;

	private String jsessionId;

	private Result resultado;

	private List<RegistroTramiteFase> registroTramiteFases= new ArrayList<RegistroTramiteFase>();

	private Date oplock;

	public RegistroTramite() {
		
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_REGISTRO_TRAMITE")
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Temporal(value = TemporalType.TIMESTAMP)
	@NotNull
	public Date getInicio() {
		return this.inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getFin() {
		return this.fin;
	}

	public void setFin(Date fin) {
		this.fin = fin;
	}

	@NotNull
	public String getSia() {
		return this.sia;
	}

	public void setSia(String sia) {
		this.sia = sia;
	}

	@Enumerated(STRING)
	public TipoAccion getAccion() {
		return this.accion;
	}

	public void setAccion(TipoAccion accion) {
		this.accion = accion;
	}

	@NotNull
	public String getJsessionId() {
		return this.jsessionId;
	}

	public void setJsessionId(String jsessionId) {
		this.jsessionId = jsessionId;
	}

	@Enumerated(STRING)
	public Result getResultado() {
		return resultado;
	}

	public void setResultado(Result resultado) {
		this.resultado = resultado;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "registroTramite", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<RegistroTramiteFase> getRegistroTramiteFases() {
		return registroTramiteFases;
	}

	public void setRegistroTramiteFases(List<RegistroTramiteFase> param) {
		this.registroTramiteFases = param;
	}

	/**
	 * @return the oplock
	 */
	@Version
	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getOplock() {
		return oplock;
	}

	/**
	 * @param oplock
	 *            the oplock to set
	 */
	public void setOplock(Date oplock) {
		this.oplock = oplock;
	}

	@PrePersist
	public void beforePersist() {
		// establecemos el resutado de la operación
//		boolean isOK = this.getRegistroTramiteFases().stream()
//				.anyMatch(f -> f.equals(RegistrationPhase.TERMINATE_OK));
		
		setResultado(Result.RESULT_KO);
		for (RegistroTramiteFase rtf : getRegistroTramiteFases()) {
			if (RegistrationPhase.TERMINATE_OK.equals(rtf.getPhase())) {
				setResultado(Result.RESULT_OK);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accion == null) ? 0 : accion.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((jsessionId == null) ? 0 : jsessionId.hashCode());
		result = prime * result + ((sia == null) ? 0 : sia.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistroTramite other = (RegistroTramite) obj;
		if (accion != other.accion)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (jsessionId == null) {
			if (other.jsessionId != null)
				return false;
		} else if (!jsessionId.equals(other.jsessionId))
			return false;
		if (sia == null) {
			if (other.sia != null)
				return false;
		} else if (!sia.equals(other.sia))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RegistroTramite [id=" + id + ", usuario=" + usuario + ", sia=" + sia + ", accion=" + accion + "]";
	}

}

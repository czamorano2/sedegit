/*
 * ============================================================================
 *                   GNU Lesser General Public License
 * ============================================================================
 *
 * Taylor - The Java Enterprise Application Framework.
 * Copyright (C) 2005 John Gilbert jgilbert01@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 * John Gilbert
 * Email: jgilbert01@users.sourceforge.net
 */
package es.imserso.sede.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.PreRemove;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * Usuario que puede acceder a uno o varios tramites
 *
 * @author 11825775
 *
 */
@Entity
@SequenceGenerator(name = "SEQ_USUARIO", initialValue = 1, allocationSize = 1, sequenceName = "SEC_USUARIO")
@Table(uniqueConstraints = { @UniqueConstraint(name = "IDX_USUARIO", columnNames = { "usuario" }) })
public class Usuario implements Serializable {

	private static final long serialVersionUID = -6028388578304524841L;

	public Usuario() {
	}

	/**
	 * ------------------------------------------ The primary key.
	 *
	 *
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_USUARIO")
	public Long getId() {
		return id;
	}


	public void setId(final Long id) {
		this.id = id;
	}


	private Long id = null;

	/**
	 * ------------------------------------------ documento de identificacion
	 * del usuario para realizar tramites
	 *
	 *
	 */
	@NotNull(message = "el usuario es obligatorio")
	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(final String usuario) {
		this.usuario = usuario;
	}


	private String usuario = null;

	/**
	 * ------------------------------------------ tramites que puede gestionar
	 * el usuario
	 *
	 *
	 */
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	public Set<Tramite> getTramites() {
		if (this.tramites == null) {
			this.tramites = new HashSet<Tramite>();
		}
		return tramites;
	}


	public void setTramites(final Set<Tramite> tramites) {
		this.tramites = tramites;
	}

	/**
	 * Associate Usuario with Tramite
	 * 
	 *
	 */
	public void addTramite(Tramite tramite) {
		if (tramite == null) {
			return;
		}
		getTramites().add(tramite);
		tramite.getUsuarios().add(this);
	}

	/**
	 * Unassociate Usuario from Tramite
	 * 
	 *
	 */
	public void removeTramite(Tramite tramite) {
		if (tramite == null) {
			return;
		}
		getTramites().remove(tramite);
		tramite.getUsuarios().remove(this);
	}

	/**
	 *
	 */
	public void removeAllTramites() {
		List<Tramite> remove = new ArrayList<Tramite>();
		remove.addAll(getTramites());
		for (Tramite element : remove) {
			removeTramite(element);
		}
	}


	private Set<Tramite> tramites = null;

	/**
	 * ------------------------------------------ The optimistic lock property.
	 *
	 *
	 */
	@Version
	public Long getOplock() {
		return oplock;
	}


	public void setOplock(final Long oplock) {
		this.oplock = oplock;
	}


	private Long oplock = null;

	// ------------------------------------------
	// Utils
	// ------------------------------------------


	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);
		builder.append("id", getId());
		builder.append("usuario", getUsuario());
		builder.append("oplock", getOplock());
		return builder.toString();
	}


	@PreRemove
	public void preRemove() {
	}


	public Usuario deepClone() throws Exception {
		Usuario clone = (Usuario) super.clone();
		clone.setId(null);

		clone.setTramites(null);
		for (Tramite kid : this.getTramites()) {
			clone.addTramite(kid);
		}
		return clone;
	}


	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((id == null) ? super.hashCode() : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Usuario))
			return false;
		final Usuario other = (Usuario) obj;
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		return true;
	}
}

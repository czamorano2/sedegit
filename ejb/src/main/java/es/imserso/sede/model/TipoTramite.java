package es.imserso.sede.model;

import es.imserso.sede.util.Global;

public enum TipoTramite {
	TURISMO("Turismo", Global.CODIGO_SIA_TURISMO),
	TERMALISMO("Termalismo", Global.CODIGO_SIA_TERMALISMO),
	PROPOSITO_GENERAL("Propósito General", Global.CODIGO_SIA_PROPOSITO_GENERAL),
	GENERICO("Genérico", "");
	
	private String nombre;
	private String sia;
	
	private TipoTramite(String nombre, String sia) {
		this.nombre = nombre;
		this.sia = sia;
	}

	public String getNombre() {
		return nombre;
	}

	public String getSia() {
		return sia;
	}

	public static TipoTramite getTipoTramite(String codigoSIA) {
		for (TipoTramite t : values()) {
			if (t.getSia().equals(codigoSIA)) {
				return t;
			}
		}
		//No es ninguno de los trámites que tienen tratamiento específico de modo que consideramos que es genérico
		return TipoTramite.GENERICO;		
	}
}

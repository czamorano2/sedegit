/*
 * ============================================================================
 *                   GNU Lesser General Public License
 * ============================================================================
 *
 * Taylor - The Java Enterprise Application Framework.
 * Copyright (C) 2005 John Gilbert jgilbert01@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 * John Gilbert
 * Email: jgilbert01@users.sourceforge.net
 */
package es.imserso.sede.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @todo add comment for javadoc
 *
 * @author 11825775
 *
 */
@Entity
public class SolicitudPdf extends PlantillaPdf implements Serializable {

	private static final long serialVersionUID = -5321214248857810456L;

	public SolicitudPdf() {
	}

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 *
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "solicitudPdf", cascade = CascadeType.MERGE, orphanRemoval = false)
	public List<Tramite> getTramites() {
		if (this.tramites == null) {
			this.tramites = new ArrayList<Tramite>();
		}
		return tramites;
	}


	public void setTramites(final List<Tramite> tramites) {
		this.tramites = tramites;
	}

	/**
	 * Associate SolicitudPdf with Tramite
	 * 
	 *
	 */
	public void addTramite(Tramite tramite) {
		if (tramite == null) {
			return;
		}
		getTramites().add(tramite);
		tramite.setSolicitudPdf(this);
	}

	/**
	 * Unassociate SolicitudPdf from Tramite
	 * 
	 *
	 */
	public void removeTramite(Tramite tramite) {
		if (tramite == null) {
			return;
		}
		getTramites().remove(tramite);
		tramite.setSolicitudPdf(null);
	}

	/**
	 *
	 */
	public void removeAllTramites() {
		List<Tramite> remove = new ArrayList<Tramite>();
		remove.addAll(getTramites());
		for (Tramite element : remove) {
			removeTramite(element);
		}
	}


	private List<Tramite> tramites = null;

	// ------------------------------------------
	// Utils
	// ------------------------------------------


	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);
		builder.append("id", getId());
		builder.append("valor", getValor());
		builder.append("nombre", getNombre());
		builder.append("descripcion", getDescripcion());
		builder.append("oplock", getOplock());
		builder.append("idioma", getIdioma());
		return builder.toString();
	}


	public SolicitudPdf deepClone() throws Exception {
		SolicitudPdf clone = (SolicitudPdf) super.deepClone();

		clone.setTramites(null);
		for (Tramite kid : this.getTramites()) {
			clone.addTramite(kid.deepClone());
		}
		return clone;
	}
}

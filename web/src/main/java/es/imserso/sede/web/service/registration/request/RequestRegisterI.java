package es.imserso.sede.web.service.registration.request;

import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.util.exception.SedeException;

/**
 * Interface para registrar una solicitud en la Sede Electrónica
 * 
 * @author 11825775
 *
 */
public interface RequestRegisterI {

	/**
	 * Registra una solicitud en la Sede Electrónica
	 * 
	 * @throws SedeException
	 */
	void register(SolicitudDTOI dto) throws SedeException;

}

package es.imserso.sede.web.service.registration.request.action.persist;

import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.inject.Inject;

import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.service.converter.impl.PdfUtil;
import es.imserso.sede.service.registration.receipt.ReceiptServiceI;
import es.imserso.sede.util.resources.ResourceQ;

/**
 * Genera y firma el PDF de la solicitud a partir del template y del Map con los
 * datos de la solicitud
 * 
 * @author 11825775
 *
 */
public class RequestPdfGenerator implements Function<PersistedRequestDTOI, ByteArrayWrapper> {

	@Inject
	@ResourceQ
	Supplier<ByteArrayWrapper> requestTemplateSupplier;

	@Inject
	@ResourceQ
	Function<SolicitudDTOI, Map<String, String>> requestTemplateDataSupplier;

	@Inject
	ReceiptServiceI receiptService;

	@Override
	public ByteArrayWrapper apply(PersistedRequestDTOI dto) {

		// obtiene el template de la base de datos
		ByteArrayWrapper template = requestTemplateSupplier.get();

		// obtiene el Map con los datos que se incrustarán en el template
		Map<String, String> data = requestTemplateDataSupplier.apply(dto.getRequestDTO());

		// genera y firma el PDF
		return new ByteArrayWrapper(receiptService.signReceipt(PdfUtil.generatePdf(data, template.getBytes())));
	}

}

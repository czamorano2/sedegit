package es.imserso.sede.web.service.registration.request.action.persist;

import java.util.Map;
import java.util.function.Function;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.data.SolicitudRepository;
import es.imserso.sede.data.TramiteRepository;
import es.imserso.sede.data.dto.solicitud.SolicitudDTO;
import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.data.dto.util.IdentificadorRegistroDTO;
import es.imserso.sede.model.Estado;
import es.imserso.sede.model.Solicitud;
import es.imserso.sede.model.TipoAutenticacion;
import es.imserso.sede.util.resources.ResourceQ;

public class DatabaseRequestPersister implements Function<SolicitudDTOI, Long> {

	private static final Logger log = Logger.getLogger(DatabaseRequestPersister.class.getName());

	@Inject
	TramiteRepository tramiteRepository;

	@Inject
	SolicitudRepository solicitudRepository;

	@Inject
	@ResourceQ
	Function<SolicitudDTOI, Map<String, String>> requestTemplateDataSupplier;

	// persiste el PDF/UA en el registro electrónico
	@Inject
	@ResourceQ
	Function<InputRegistryData, IdentificadorRegistroDTO> requestRegistryPersister;

	@Override
	public Long apply(SolicitudDTOI dto) {
		
		log.debug("applying database persistence...");

		Solicitud solicitudInstance = dto.extractNewSolicitud();

		// FIXME esta información nos la debería de pasar el UCM
		solicitudInstance.setTipoAutenticacion(TipoAutenticacion.CLAVE_CERTIFICADO);

		solicitudInstance.setEstado(Estado.getEstadoInicial(dto.getCodigoSIA()));
		solicitudInstance.setTramite(tramiteRepository.findBySIA(dto.getCodigoSIA()));
		solicitudInstance.setDto((SolicitudDTO) dto);
		
		return solicitudRepository.save(solicitudInstance);
	}

}

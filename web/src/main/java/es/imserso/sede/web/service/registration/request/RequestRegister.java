/**
 * 
 */
package es.imserso.sede.web.service.registration.request;

import java.util.function.Consumer;
import java.util.function.Function;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.data.dto.util.IdentificadorRegistroDTO;
import es.imserso.sede.util.exception.SedeException;
import es.imserso.sede.util.resources.ResourceQ;
import es.imserso.sede.web.service.registration.request.action.persist.ByteArrayWrapper;
import es.imserso.sede.web.service.registration.request.action.persist.PersistedRequestDTO;
import es.imserso.sede.web.service.registration.request.action.persist.PersistedRequestDTOI;

/**
 * Registra una solicitud de un trámite.
 * 
 * @author 11825775
 *
 */
public class RequestRegister implements RequestRegisterI {

	private static Logger log = Logger.getLogger(RequestRegister.class.getName());

	@Inject
	@ResourceQ
	Consumer<SolicitudDTOI> requestValidator;
	
	
	@Inject
	@ResourceQ
	Function<PersistedRequestDTOI, ByteArrayWrapper> pdfGenerator;

	@Inject
	@ResourceQ
	Function<PersistedRequestDTOI, IdentificadorRegistroDTO> registryPersister;

	@Inject
	@ResourceQ
	Function<SolicitudDTOI, Long> databasePersister;

	@Inject
	@ResourceQ
	Consumer<PersistedRequestDTOI> databaseUpdater;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.web.service.registration.request.RequestRegister#register()
	 */
	@Override
	public void register(SolicitudDTOI dto) throws SedeException {

		log.info("registering request ...");

		PersistedRequestDTOI persistedDTO = new PersistedRequestDTO();

		requestValidator.accept(dto);

		persistedDTO.setRequestDatabaseId(databasePersister.apply(dto));
		
		persistedDTO.setRequestPdf(pdfGenerator.apply(persistedDTO).getBytes());

		persistedDTO.setIdentificadorRegistroElectronico(registryPersister.apply(persistedDTO));
		
//		persistedDTO.setReceiptPdf(bytes);

		databaseUpdater.accept(persistedDTO);

	}

}

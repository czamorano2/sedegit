package es.imserso.sede.web.util.resources;

import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.enterprise.inject.Produces;

import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.data.dto.util.IdentificadorRegistroDTO;
import es.imserso.sede.util.cdi.UtilsCDI;
import es.imserso.sede.util.resources.ResourceQ;
import es.imserso.sede.web.service.registration.request.action.persist.ByteArrayWrapper;
import es.imserso.sede.web.service.registration.request.action.persist.DatabaseRequestPersister;
import es.imserso.sede.web.service.registration.request.action.persist.InputRegistryData;
import es.imserso.sede.web.service.registration.request.action.persist.InveSicresRequestPersister;
import es.imserso.sede.web.service.registration.request.action.persist.PersistedRequestDTOI;
import es.imserso.sede.web.service.registration.request.action.persist.RegistryPersister;
import es.imserso.sede.web.service.registration.request.action.persist.RequestPdfGenerator;
import es.imserso.sede.web.service.registration.request.action.persist.RequestTemplateDataMap;
import es.imserso.sede.web.service.registration.request.action.persist.RequestTemplateSupplier;
import es.imserso.sede.web.service.registration.request.action.validation.GenericoDtoValidator;
import es.imserso.sede.web.service.registration.request.action.validation.PropositoGeneralDtoValidator;
import es.imserso.sede.web.service.registration.request.action.validation.TermalismoDtoValidator;
import es.imserso.sede.web.service.registration.request.action.validation.TurismoDtoValidator;
import es.imserso.sede.web.util.route.ParamValues;

public class Resources {

	@ResourceQ
	@SuppressWarnings("unchecked")
	@Produces
	public Consumer<SolicitudDTOI> getRequestValidator(ParamValues paramValues) {
		if (paramValues.isTurismo()) {
			return (Consumer<SolicitudDTOI>) UtilsCDI.getBeanByReference(TurismoDtoValidator.class);

		} else if (paramValues.isTermalismo()) {
			return (Consumer<SolicitudDTOI>) UtilsCDI.getBeanByReference(TermalismoDtoValidator.class);

		} else if (paramValues.isPropositoGeneral()) {
			return (Consumer<SolicitudDTOI>) UtilsCDI.getBeanByReference(PropositoGeneralDtoValidator.class);

		} else {
			return (Consumer<SolicitudDTOI>) UtilsCDI.getBeanByReference(GenericoDtoValidator.class);

		}
	}

	@SuppressWarnings("unchecked")
	@ResourceQ
	@Produces
	public Function<SolicitudDTOI, Long> getDatabasePersister() {
		// Si hubiese más implementaciones, este es el sitio donde implementar la lógica
		// de negocio que decida cual es el componente adecuado
		return (Function<SolicitudDTOI, Long>) UtilsCDI.getBeanByReference(DatabaseRequestPersister.class);
	}

	@SuppressWarnings("unchecked")
	@ResourceQ
	@Produces
	public Function<PersistedRequestDTOI, ByteArrayWrapper> getPdfGenerator() {
		// Si hubiese más implementaciones, este es el sitio donde implementar la lógica
		// de negocio que decida cual es el componente adecuado
		return (Function<PersistedRequestDTOI, ByteArrayWrapper>) UtilsCDI
				.getBeanByReference(RequestPdfGenerator.class);
	}

	@SuppressWarnings("unchecked")
	@ResourceQ
	@Produces
	public Function<PersistedRequestDTOI, IdentificadorRegistroDTO> getRegistryPersister() {
		// Si hubiese más implementaciones, este es el sitio donde implementar la lógica
		// de negocio que decida cual es el componente adecuado
		return (Function<PersistedRequestDTOI, IdentificadorRegistroDTO>) UtilsCDI
				.getBeanByReference(RegistryPersister.class);
	}

	@SuppressWarnings("unchecked")
	@ResourceQ
	@Produces
	public Supplier<ByteArrayWrapper> getRequestTemplateSupplier() {
		// Si hubiese más implementaciones, este es el sitio donde implementar la lógica
		// de negocio que decida cual es el componente adecuado
		return (Supplier<ByteArrayWrapper>) UtilsCDI.getBeanByReference(RequestTemplateSupplier.class);
	}

	@SuppressWarnings("unchecked")
	@ResourceQ
	@Produces
	public Function<SolicitudDTOI, Map<String, String>> getRequestTemplateDataSupplier() {
		// Si hubiese más implementaciones, este es el sitio donde implementar la lógica
		// de negocio que decida cual es el componente adecuado
		return (Function<SolicitudDTOI, Map<String, String>>) UtilsCDI.getBeanByReference(RequestTemplateDataMap.class);
	}

	@SuppressWarnings("unchecked")
	@ResourceQ
	@Produces
	public Function<InputRegistryData, IdentificadorRegistroDTO> getRequestRegistryPersister() {
		// Si hubiese más implementaciones, este es el sitio donde implementar la lógica
		// de negocio que decida cual es el componente adecuado
		return (Function<InputRegistryData, IdentificadorRegistroDTO>) UtilsCDI
				.getBeanByReference(InveSicresRequestPersister.class);
	}

}

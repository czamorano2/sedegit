package es.imserso.sede.web.service.registration.request.action.validation;

import java.util.Set;

import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolation;
import javax.validation.constraints.NotNull;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.impl.GenericoDTO;
import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.data.validation.group.GroupNotRepresentante;
import es.imserso.sede.data.validation.group.GroupRepresentante;
import es.imserso.sede.util.exception.ValidationException;

/**
 * Validador de {@link es.imserso.sede.data.dto.impl.GenericoDTO}
 * 
 * @author 11825775
 *
 */
@Dependent
public class GenericoDtoValidator extends DtoValidator {

	private static final Logger log = Logger.getLogger(GenericoDtoValidator.class);

	public void accept(SolicitudDTOI dto) {
		validated = Boolean.TRUE;

		log.info("validando los datos del formuario ...");
		validateFormData((GenericoDTO) dto);

		log.info("validando fechas de tramitación...");
		UCMservice.validateProcedureTerms(dto.getCodigoSIA());

		validatedOK = Boolean.TRUE;
		validatedKO = Boolean.FALSE;

	}

	/**
	 * Ejecuta todas las validaciones necesarias para comprobar que los datos
	 * introcucidos son válidos
	 * 
	 * @return verdadero si pasa todas las validaciones
	 */
	private void validateFormData(@NotNull GenericoDTO dto) throws ValidationException {

		int constraintViolationCounter = 0;

		// validamos los campos del formulario
		Set<ConstraintViolation<GenericoDTO>> constraintViolations = validator.validate((GenericoDTO) dto);

		if (constraintViolations.size() > 0) {
			log.warn("se han encontrado " + constraintViolations.size() + " errores de validación en el formulario:");
			for (ConstraintViolation<GenericoDTO> violation : constraintViolations) {
				constraintViolationCounter++;
				log.warn(violation.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
						violation.getPropertyPath().toString(), violation.getMessage()));
			}

		}

		if (paramValues.isRepresentante()) {
			log.debug("validando campos cuando hay representante...");
			Set<ConstraintViolation<GenericoDTO>> constraintViolationsRepresentante = validator
					.validate((GenericoDTO) dto, GroupRepresentante.class);
			log.warn("se han encontrado " + constraintViolationsRepresentante.size()
					+ " errores de validación de representante en el formulario:");
			for (ConstraintViolation<GenericoDTO> violation : constraintViolationsRepresentante) {

				constraintViolationCounter++;
				log.warn(violation.getPropertyPath() + ": " + violation.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
						violation.getPropertyPath().toString(), violation.getMessage()));
			}

		} else {
			log.debug("validando campos cuando no hay representante...");
			Set<ConstraintViolation<GenericoDTO>> constraintViolationsNotRepresentante = validator
					.validate((GenericoDTO) dto, GroupNotRepresentante.class);
			log.warn("se han encontrado " + constraintViolationsNotRepresentante.size()
					+ " errores de validación de NO representante en el formulario:");
			for (ConstraintViolation<GenericoDTO> violation : constraintViolationsNotRepresentante) {

				constraintViolationCounter++;
				log.warn(violation.getPropertyPath() + ": " + violation.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
						violation.getPropertyPath().toString(), violation.getMessage()));
			}
		}

		if (constraintViolationCounter > 0) {
			if (FacesContext.getCurrentInstance().isValidationFailed()) {
				log.info("isValidationFailed = TRUE");
			}
			throw new ValidationException(String.format("Se han encontrado %d violaciones de reglas en el formulario",
					constraintViolationCounter));
		}

	}

	/**
	 * @return <code>true</code> si se ha realizado ya la validación del DTO
	 */
	public boolean alreadyValidated() {
		return validated;
	}

	/**
	 * @return <code>true</code> si ha pasado correctamente todas las validaciones o
	 *         <code>false</code> si aún no se ha validado o si no ha pasado
	 *         correctamente alguna validación
	 */
	public boolean isValid() {
		return validatedOK;
	}

	/**
	 * @return <code>true</code> si NO ha pasado correctamente todas las
	 *         validaciones o <code>false</code> si SI ha pasado correctamente todas
	 *         las validaciones
	 */
	public boolean isInvalid() {
		return validatedKO;
	}

}

package es.imserso.sede.web.service.registration.request.action.validation;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolation;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.imserso.sede.data.dto.impl.turismo.TurismoDTO;
import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.data.validation.group.GroupNotRepresentante;
import es.imserso.sede.data.validation.group.GroupRepresentante;
import es.imserso.sede.service.registration.RegistrationException;
import es.imserso.sede.util.DateUtil;
import es.imserso.sede.util.exception.ValidationException;

public class TurismoDtoValidator extends DtoValidator {

	private static final Logger log = Logger.getLogger(TurismoDtoValidator.class.getName());

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.imserso.sede.web.model.validator.DtoValidatorI#validate(es.imserso.sede.
	 * data.dto.PersonaInteresadaDTOI)
	 */
	@Override
	public void accept(SolicitudDTOI dto) {
		validated = Boolean.TRUE;

		// Hermes debe estar respondiendo para validar el formulario. De lo contrario no
		// se registra la solicitud en la sede
		validaFechaOficialEnPlazo();

		log.debug("validando los datos del formuario ...");
		validateFormData((TurismoDTO) dto);

		log.debug("validando fechas de tramitación...");
		UCMservice.validateProcedureTerms(dto.getCodigoSIA());

		log.debug("validando denegación de autorización de consulta de datos...");
		validateAuthenticationDenial((TurismoDTO) dto);

		validatedOK = Boolean.TRUE;
		validatedKO = Boolean.FALSE;

	}

	/**
	 * Si el usuario ha marcado la casilla, se le pedirá un adjunto por cada
	 * persona, otro si tiene familia numerosa y, al menos, otro por cada pensión.
	 * 
	 * @param dto
	 * @throws ValidationException
	 */
	private void validateAuthenticationDenial(TurismoDTO dto) throws ValidationException {
		int minAttachments = 0;

		if (dto.hasConyuge()) {
			minAttachments++;
		}

		if (dto.hasHijo()) {
			minAttachments++;
		}

		if (dto.hasVinculacion()) {
			minAttachments++;
		}

		if (dto.hasFamiliaNumerosa()) {
			minAttachments++;
		}

		minAttachments += dto.datosEconomicosCount();

		if (dto.getDenegarAutorizacionConsultas() && dto.getAttachedFiles().size() < minAttachments) {
			String errmsg = "no se han adjuntado todos los documentos solicitados";
			log.warn(errmsg);
			throw new ValidationException(errmsg);
		}

	}

	/**
	 * Ejecuta todas las validaciones necesarias para comprobar que los datos
	 * introcucidos son válidos
	 * 
	 * @return verdadero si pasa todas las validaciones
	 */
	private void validateFormData(@NotNull TurismoDTO dto) throws ValidationException {

		int constraintViolationCounter = 0;

		// validamos los campos del formulario
		Set<ConstraintViolation<TurismoDTO>> constraintViolations = validator.validate((TurismoDTO) dto);

		constraintViolationCounter += constraintViolations.size();

		if (constraintViolations.size() > 0) {

			log.warn("se han encontrado " + constraintViolations.size() + " errores de validación en el formulario:");

			constraintViolations.stream().forEach(violation -> {

				log.warn(violation.getMessage());

				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						violation.getPropertyPath().toString(), violation.getMessage()));
			});
		}

		if (paramValues.isRepresentante()) {
			log.debug("validando campos cuando hay representante...");
			Set<ConstraintViolation<TurismoDTO>> constraintViolationsRepresentante = validator
					.validate((TurismoDTO) dto, GroupRepresentante.class);
			if (constraintViolationsRepresentante.isEmpty()) {
				log.debug("OK");

			} else {

				log.warnv("se han encontrado {0} errores de validación de representante en el formulario:",
						constraintViolationsRepresentante.size());

				constraintViolationCounter += constraintViolationsRepresentante.size();

				constraintViolationsRepresentante.stream().forEach(violation -> {

					log.warn(violation.getPropertyPath() + ": " + violation.getMessage());

					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
							violation.getPropertyPath().toString(), violation.getMessage()));

				});

			}

		} else {

			log.debug("validando campos cuando no hay representante...");

			Set<ConstraintViolation<TurismoDTO>> constraintViolationsNotRepresentante = validator
					.validate((TurismoDTO) dto, GroupNotRepresentante.class);

			if (constraintViolationsNotRepresentante.isEmpty()) {

				log.debug("OK");

			} else {

				log.warn("se han encontrado " + constraintViolationsNotRepresentante.size()
						+ " errores de validación de NO representante en el formulario:");

				constraintViolationCounter += constraintViolationsNotRepresentante.size();

				constraintViolationsNotRepresentante.stream().forEach(violation -> {

					log.warn(violation.getPropertyPath() + ": " + violation.getMessage());

					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
							violation.getPropertyPath().toString(), violation.getMessage()));
				});
			}
		}

		if (constraintViolationCounter > 0) {
			if (FacesContext.getCurrentInstance().isValidationFailed()) {
				log.info("isValidationFailed = TRUE");
			}
			throw new ValidationException(String.format("Se han encontrado %d violaciones de reglas en el formulario",
					constraintViolationCounter));
		}

	}

	/**
	 * Valida si la fecha oficial (cuando entrega la solicitud) está dentro de las
	 * fechas del plazo actual de entrega de solicitudes
	 * 
	 * @throws RegistrationException
	 */
	private void validaFechaOficialEnPlazo() throws ValidationException {
		// leemos el plazo del turno por defecto de Hermes en formato JSON
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(propertyComponent.getTurismoResourcesURL() + "plazoturnopordefecto");
		Response response = target.request().get();
		// Comprobamos si la respuesta es ok porque si en este momento no hay
		// plazo devolverá un código de forbidden
		if (response.getStatusInfo() != Response.Status.OK) {
			throw new ValidationException("Error al validar la fecha oficial en plazo");
		}
		String plazoDTOString = response.readEntity(String.class);
		log.debug("plazoDTOString=" + plazoDTOString);
		response.close();

		// Parseamos las fechas recibidas
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode rootNode;
		JsonNode fechaInicioEntregaSolicitudNode;
		JsonNode fechaFinEntregaSolicitudNode;
		try {
			rootNode = objectMapper.readTree(plazoDTOString);
			fechaInicioEntregaSolicitudNode = rootNode.path("fechaInicioEntregaSolicitud");
			fechaFinEntregaSolicitudNode = rootNode.path("fechaFinEntregaSolicitud");
			log.debug("fechaInicioEntregaSolicitud=" + fechaInicioEntregaSolicitudNode.asText());
			log.debug("fechaFinEntregaSolicitudNode=" + fechaFinEntregaSolicitudNode.asText());
		} catch (IOException e) {
			String errmsg = "No se ha podido recuperar el plazo actual para saber las fechas de entrega";
			log.error(errmsg);
			throw new ValidationException(errmsg);
		}

		Date fechaInicioEntregaSolicitud;
		Date fechaFinEntregaSolicitud;
		try {
			fechaInicioEntregaSolicitud = DateUtil.parseDateGuiones(fechaInicioEntregaSolicitudNode.asText());
			fechaFinEntregaSolicitud = DateUtil.parseDateGuiones(fechaFinEntregaSolicitudNode.asText());
		} catch (ParseException e) {
			String errmsg = "No se han podido convertir correctamente las fechas del plazo";
			log.error(errmsg);
			throw new ValidationException(errmsg);
		}

		// leemos la fecha oficial de la Sede
		Date officialDate = DateUtil.getOfficialDate();
		log.debug("officialDate=" + officialDate);

		// validación de fechas
		if (officialDate.before(fechaInicioEntregaSolicitud) || officialDate.after(fechaFinEntregaSolicitud)) {
			String errmsg = "La fecha oficial no está dentro de las fechas del plazo.";
			log.error(errmsg);
			throw new ValidationException(errmsg);
		}

		log.debug("Validada correctamente fecha oficial en plazo");
	}

}

package es.imserso.sede.web.service.registration.request.action.validation;

import java.util.Set;

import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolation;
import javax.validation.constraints.NotNull;

import org.jboss.logging.Logger;

import es.imserso.sede.data.dto.impl.termalismo.TermalismoDTO;
import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;
import es.imserso.sede.data.validation.group.GroupNotRepresentante;
import es.imserso.sede.data.validation.group.GroupRepresentante;
import es.imserso.sede.util.exception.ValidationException;

/**
 * Validador de {@link es.imserso.sede.data.dto.impl.TermalismoDto}
 * 
 * @author 11825775
 *
 */
@Dependent
public class TermalismoDtoValidator extends DtoValidator {

	private static final Logger log = Logger.getLogger(TermalismoDtoValidator.class.getName());

	/**
	 * Realiza las validaciones oportunas para asegurar que los datos del DTO son
	 * correctos
	 * <p>
	 * Siempre se ejecutará antes del método
	 * {@link es.imserso.sede.web.view.alta.SolicitudTermalismoView#save()}
	 * 
	 * @throws ValidationException
	 */
	public void accept(SolicitudDTOI dto) {
		validated = Boolean.TRUE;

		log.info("validando los datos del formuario ...");
		datosCriticos((TermalismoDTO) dto);
		validateFormData((TermalismoDTO) dto);
		log.info("validando fechas de tramitación...");
		UCMservice.validateProcedureTerms(dto.getCodigoSIA());

		validatedOK = Boolean.TRUE;
		validatedKO = Boolean.FALSE;

	}

	private void datosCriticos(TermalismoDTO dto) {
		// al menos un balneario es obligatorio
		if (dto.getListaBalnearios() == null || dto.getListaBalnearios().isEmpty()) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe seleccionar al menos un balneario", ""));
		}
		// al menos un balneario es obligatorio
		if (dto.getListaTurnos() == null || dto.getListaTurnos().isEmpty()) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe seleccionar al menos un turno", ""));
		}

	}

	/**
	 * Ejecuta todas las validaciones necesarias para comprobar que los datos
	 * introcucidos son válidos
	 * 
	 * @return verdadero si pasa todas las validaciones
	 */
	private void validateFormData(@NotNull TermalismoDTO dto) throws ValidationException {

		int constraintViolationCounter = 0;

		// validamos los campos del formulario
		Set<ConstraintViolation<TermalismoDTO>> constraintViolations = validator.validate((TermalismoDTO) dto);

		if (constraintViolations.size() > 0) {
			log.warn("se han encontrado " + constraintViolations.size() + " errores de validación en el formulario:");
			for (ConstraintViolation<TermalismoDTO> violation : constraintViolations) {
				constraintViolationCounter++;
				log.warn(violation.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						violation.getPropertyPath().toString(), violation.getMessage()));
			}

		}

		if (paramValues.isRepresentante()) {
			log.debug("validando campos cuando hay representante...");
			Set<ConstraintViolation<TermalismoDTO>> constraintViolationsRepresentante = validator
					.validate((TermalismoDTO) dto, GroupRepresentante.class);
			log.warn("se han encontrado " + constraintViolationsRepresentante.size()
					+ " errores de validación de representante en el formulario:");
			for (ConstraintViolation<TermalismoDTO> violation : constraintViolationsRepresentante) {

				constraintViolationCounter++;
				log.warn(violation.getPropertyPath() + ": " + violation.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						violation.getPropertyPath().toString(), violation.getMessage()));
			}

		} else {
			log.debug("validando campos cuando no hay representante...");
			Set<ConstraintViolation<TermalismoDTO>> constraintViolationsNotRepresentante = validator
					.validate((TermalismoDTO) dto, GroupNotRepresentante.class);
			log.warn("se han encontrado " + constraintViolationsNotRepresentante.size()
					+ " errores de validación de NO representante en el formulario:");
			for (ConstraintViolation<TermalismoDTO> violation : constraintViolationsNotRepresentante) {

				constraintViolationCounter++;
				log.warn(violation.getPropertyPath() + ": " + violation.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						violation.getPropertyPath().toString(), violation.getMessage()));
			}
		}

		if (constraintViolationCounter > 0) {
			if (FacesContext.getCurrentInstance().isValidationFailed()) {
				log.info("isValidationFailed = TRUE");
			}
			throw new ValidationException(String.format("Se han encontrado %d violaciones de reglas en el formulario",
					constraintViolationCounter));
		}

	}

	/**
	 * @return <code>true</code> si se ha realizado ya la validación del DTO
	 */
	public boolean alreadyValidated() {
		return validated;
	}

	/**
	 * @return <code>true</code> si ha pasado correctamente todas las validaciones o
	 *         <code>false</code> si aún no se ha validado o si no ha pasado
	 *         correctamente alguna validación
	 */
	public boolean isValid() {
		return validatedOK;
	}

	/**
	 * @return <code>true</code> si NO ha pasado correctamente todas las
	 *         validaciones o <code>false</code> si SI ha pasado correctamente todas
	 *         las validaciones
	 */
	public boolean isInvalid() {
		return validatedKO;
	}

}

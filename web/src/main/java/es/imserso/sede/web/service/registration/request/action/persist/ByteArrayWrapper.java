package es.imserso.sede.web.service.registration.request.action.persist;

/**
 * Clase para devolver un array de bytes desde un Supplier o pasarlo a un
 * Consumer
 * 
 * @author 11825775
 *
 */
public class ByteArrayWrapper {

	private byte[] bytes;
	
	public ByteArrayWrapper() {
	}
	
	public ByteArrayWrapper(byte[] bytes) {
		this.bytes = bytes;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}
}

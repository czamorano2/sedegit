package es.imserso.sede.web.service.registration.request.action.persist;

import java.util.function.Function;

import es.imserso.sede.data.dto.solicitud.SolicitudDTOI;

/**
 * Consumer que persiste el DTO de la solicitud en la base de datos
 * 
 * @author 11825775
 *
 */
public class DatabasePersister implements Function<SolicitudDTOI, Long> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.function.Consumer#accept(java.lang.Object)
	 */
	@Override
	public Long apply(SolicitudDTOI t) {
		// genera la instancia de Solicitud que se va a persistir
		
		
		
		// genera el XML con los datos del DTO desmaterilizados de forma que se pueda
		// volver a materializar el DTO en caso de necesidad
		
		

		// asigna el XML a la instancia de Solicitud que se va a persistir
		
		

		// persiste la instancia Solicitud
		
		
		
		return null;

	}

}
